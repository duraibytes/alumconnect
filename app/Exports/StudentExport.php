<?php

namespace App\Exports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class StudentExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Student::all();
    }

    public function headings() :array
    {
        return ["Id", "Type", "Category","Name", "Fname", "Email", "Email_verified_at", 'Password', 
                'Remember_token', 'Referred_by_user_id', 'Status', 'Created_at', 'Updated_at', 
                'Last_login', 'country_id' ];
    }
}
