<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordMail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }

    public function authenticated(Request $request, $user) {
        $user->last_login = Carbon::now()->toDateTimeString();
        $user->save();
    }

    public function custome_password_reset( Request $request ) {
        
        $data[ 'email' ]                = $request->email;
        $message                        = [ 'email.exists' => 'Email address not registered or invalid email' ];
        $validation                      = Validator::make( $data, [
            'email' => [
                'required',
                Rule::exists('users', 'email'),
            ], 
        ], $message );
        
        
        if( $validation->fails() ) {
            $error_msg                   = $validation->errors()->all();
            return redirect()->back()->withErrors( $error_msg );
        }
        $user_info                      = User::where( [ 'email' => $request->email ] )->first();
        $gen_password                   = Str::random( 10 );
        
        $password                       = Hash::make( $gen_password );
        $user_info->password            = $password;
        $user_info->save();
        // return (new PasswordMail( $gen_password, 'reset', $user_info->email ) )->render();
        Mail::to( $user_info->email )->send( new PasswordMail( $gen_password, 'reset', $user_info->email ) );
        return redirect( 'login' )->with( [ 'status' => 'Password Successfully Reset and sent to Registered email address, Pls check your inbox' ]);
        //trigger password to mail

    }
}
