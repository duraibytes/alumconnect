<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;
class CategoryController extends Controller
{
    function index( Request $request ) {
        $title                          = 'Category';
        $info                           = Category::where( 'categories.type','!=', "TAG" )
                                            ->groupBY( 'categories.type' )
                                            ->orderBy( 'categories.type', 'ASC' )
                                            ->get();
        
        return view( 'pages.category.index', [ 'title' => $title, 'info' => $info ] );
    }

    function add_form( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $parent_id                      = $request->parent_id;
        $type                           = $request->type;
        $title                          = 'Add Category';
        
        return view( 'pages.category.form.add', [ 'title' => $title, 'parent_id' => $parent_id, 'type' => $type ] );
    }

    function insert_category( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 
        
        $category_validator                 = [
                                                'name'             => [ 'required', 'max:255' ],
                                            ];
       
        //Validate the product
        $validation                         = Validator::make( $request->all(), $category_validator );
        if( $validation->fails() ) {
            $error_msg                      = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }

        $parent_id                          = $request->parent_id;
        $type                               = $request->type;
        
        $category_info[ 'name' ]            = $request->name;
        $category_info[ 'type' ]            = $type;
        $category_info[ 'parent_id' ]       = $parent_id;
        $category                           = Category::create( $category_info );
        
        return response()->json( [ 'success' => 'Category is created successfully.' ] );
    }

    function edit_form( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $category_id                    = $request->category_id;
        $category_info                  = Category::find( $category_id );
        $title                          = 'Edit Category';
        
        return view( 'pages.category.form.edit', [ 'category_info' => $category_info, 'title' => $title, 'id' => $category_id ] );
    }

    function update_category( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 
        
        $category_validator                 = [
                                                    'name'             => [ 'required', 'max:255' ],
                                                ];
       
        //Validate the product
        $validation                         = Validator::make( $request->all(), $category_validator );
        if( $validation->fails() ) {
            $error_msg                      = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }
        $check                              = Category::where( 'name', $request->name )->first();
        if( isset( $check->id ) && ( $check->id != $request->id ) )  {
            return response()->json( [ 'failure' => ['<p>Category is already exists</p>'] ] );
        }
        $id                                 = $request->id;
        
        $category_info                      = Category::find( $id );
        $category_info->name                = $request->name;
       
        $category_info->save();
        
        return response()->json( [ 'success' => 'Category is updated successfully.' ] );
    }
}
