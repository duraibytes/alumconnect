<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index( Request $request )
    {
        
        $data[ 'login_name' ]           = $request->user()->name;
        $data[ 'login_email' ]          = $request->user()->email;
        $title                          = 'Dashboard';
        $is[ 'is_drop_menu' ]           = 'yes';

        $endDateTime                    = Carbon::now();
        $startDateTime                  = Carbon::now()->subMonth(12);

        $students_type_chart            = Student::select( DB::raw('MONTH(created_at) as month'), 'category', DB::raw('sum(category = 1) future_count'), DB::raw('sum(category = 2) alumni_count'), DB::raw('sum(category = 3) current_count'))->where( 'referral_status', '<>', 'REFERRED' )->where( 'created_at', '>=', $startDateTime )->where('created_at', '<=', $endDateTime )->groupBy( 'month', 'category' )->get();
        // dd( $students_type_chart ); //->where( 'status', 1 ) 

        $startDate                      = date( 'Y-m-d', strtotime( $startDateTime ) );
        $endDate                        = date( 'Y-m-d', strtotime( $endDateTime ) );
        while( $startDate < $endDate ) {
            $signup_months[]            = date( 'M', strtotime( $startDate ) );
            $startDate                  = date("Y-m-d", strtotime("+1 month", strtotime( $startDate ) ));
        }

        $future_count                           = [0,0,0,0,0,0,0,0,0,0,0,0];
        $alumni_count                           = [0,0,0,0,0,0,0,0,0,0,0,0];
        $current_count                          = [0,0,0,0,0,0,0,0,0,0,0,0];
        foreach( $students_type_chart as $order ) {
            $future_count[$order->month-1]      += ( ( !is_null( $order->future_count ) ) ? $order->future_count : 0 );
            $alumni_count[$order->month-1]      += ( ( !is_null( $order->alumni_count ) ) ? $order->alumni_count : 0 );
            $current_count[$order->month-1]     += ( ( !is_null( $order->current_count ) ) ? $order->current_count : 0 );
        }

        $no_verified_students           = Student::where( 'status', 1 )->count();
        $no_referral_payout             = Student::where( 'referral_payment_status', '1' )->count();
        $no_blogs                       = Post::where( 'type', 'BLOG' )->where( 'status', 'PUBLISHED' )->count();
        $no_broadcast                   = Post::where( 'type', 'BROADCAST' )->where( 'status', 'PUBLISHED' )->count();
        
        return view( 'pages.dashboard.home', [ 
                                                'user_info' => $data, 
                                                'title' => $title, 
                                                'action' => $is, 
                                                'no_verified_students' => $no_verified_students, 
                                                'no_referral_payout' => $no_referral_payout, 
                                                'no_blogs' => $no_blogs, 
                                                'no_broadcast' => $no_broadcast, 
                                                'future_count' => json_encode( $future_count ), 
                                                'alumni_count' => json_encode( $alumni_count ), 
                                                'current_count' => json_encode( $current_count ), 
                                                'signup_months' => json_encode( $signup_months ) 
                                            ] );
        
    }

    public function list( Request $request ) {
        include '../../lib/class-list-util.php';

        $data = $alldata = json_decode(file_get_contents('../datasource/default.json'));

        $datatable = array_merge(array('pagination' => array(), 'sort' => array(), 'query' => array()), $_REQUEST);

        // search filter by keywords
        $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch']) ? $datatable['query']['generalSearch'] : '';
        if (!empty($filter)) {
            $data = array_filter($data, function ($a) use ($filter) {
                return (boolean)preg_grep("/$filter/i", (array)$a);
            });
            unset($datatable['query']['generalSearch']);
        }

        // filter by field query
        $query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;
        if (is_array($query)) {
            $query = array_filter($query);
            foreach ($query as $key => $val) {
                $data = list_filter($data, array($key => $val));
            }
        }

        $sort = !empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'asc';
        $field = !empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'RecordID';

        $meta = array();
        $page = !empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
        $perpage = !empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;

        $pages = 1;
        $total = count($data); // total items in array

        // sort
        usort($data, function ($a, $b) use ($sort, $field) {
            if (!isset($a->$field) || !isset($b->$field)) {
                return false;
            }

            if ($sort === 'asc') {
                return $a->$field > $b->$field ? true : false;
            }

            return $a->$field < $b->$field ? true : false;
        });

        // $perpage 0; get all data
        if ($perpage > 0) {
            $pages = ceil($total / $perpage); // calculate total pages
            $page = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
            $page = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ($page - 1) * $perpage;
            if ($offset < 0) {
                $offset = 0;
            }

            $data = array_slice($data, $offset, $perpage, true);
        }

        $meta = array(
            'page' => $page,
            'pages' => $pages,
            'perpage' => $perpage,
            'total' => $total,
        );

        // if selected all records enabled, provide all the ids
        if (isset($datatable['requestIds']) && filter_var($datatable['requestIds'], FILTER_VALIDATE_BOOLEAN)) {
            $meta['rowIds'] = array_map(function ($row) {
                foreach ($row as $first) break;
                return $first;
            }, $alldata);
        }


        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $result = array(
            'meta' => $meta + array(
                    'sort' => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );

        echo json_encode($result, JSON_PRETTY_PRINT);
    }
}
