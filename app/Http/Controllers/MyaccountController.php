<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordMail;
use App\Mail\ChangeEmail;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Crypt;


class MyaccountController extends Controller
{
    function index( Request $request ) {
        $title                          = 'My Account';
        $user_id                        = Auth::user()->id;
        $user_info                      = User::with( 'category_info' )->find( $user_id );
        return view( 'pages.myaccount.index', [ 'title' => $title, 'user_info' => $user_info ] );
    }

    public function password_reset_user( Request $request, $email ) {
        $user_info                      = User::where( [ 'email' => $email ] )->first();
        $gen_password                   = Str::random( 10 );
        
        $password                       = Hash::make( $gen_password );
        $user_info->password            = $password;
        $user_info->save();
        // return (new PasswordMail( $gen_password, 'reset' ) )->render();
        Mail::to( $user_info->email )->send( new PasswordMail( $gen_password, 'reset' ) );
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect( 'login' )->with( [ 'status' => 'Password Successfully Reset and sent to Registered email address, Pls check your inbox' ]);
        //trigger password to mail

    }

    function change_email_form( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $id                             = $request->id;
        $user_info                      = User::find( $id );
        
        return view( 'pages.myaccount.change_email', [ 'user_info' => $user_info, 'id' => $id ] );
    }

    function change_email( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 
        $user_validator   = [
            'email'             => [ 'required', 'unique:users', 'email' ],
            'password'          => [ 'required' ],
            
            
        ];
        //Validate the product
        $validation                      = Validator::make( $request->all(), $user_validator );
        if( $validation->fails() ) {
            $error_msg                          = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }
        $id                                     = $request->id;
        $user_info                              = User::find( $id );

        if( Hash::check( $request->password, $user_info->password ) ) {
            $email                              = $request->email;
            $enc_email                          = Crypt::encryptString( $email );
            
            
            $user_info->genereted_email_tokens  = $enc_email;
            $user_info->save();

            if( isset( $enc_email ) ) {
                Mail::to( $email )->send( new ChangeEmail( $enc_email ) ); 
            }
            return response()->json( [ 'success' => 'Require email confirmation, check your inbox.' ] );
            // They match
        } else {
            // They don't match
            $error_msg                   = 'Password is invalid please try again with correct one';
            return response()->json( [ 'failure' => $error_msg ] );
        }
        
    }

    public function generate_verify_email( Request $request, $email_token ) {
        
        $decrypted                                  = Crypt::decryptString($email_token);
        $user_info                                  = User::where( 'genereted_email_tokens', $email_token )->first();
        if( isset( $user_info ) && !empty( $user_info ) ) {
            
            $user_info->email                       = $decrypted;
            $user_info->genereted_email_tokens      = '';
            $user_info->save();
            Auth::logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return redirect( 'login' )->with( [ 'status' => 'Email verified successfully, please login to continue' ] );
        }
    }

    public function profile_photo_change( Request $request ) {
        $id                             = $request->id;
        $user_info                      = User::find( $id );
        
        if( $request->hasFile( 'profile_avatar' ) ) {
            $file                       = $request->file( 'profile_avatar' )->store( 'profile', 's3' );
            // set visibility for public
            Storage::disk( 's3' )->setVisibility( $file, 'public' );
            // $file                       = Storage::putFile( 'post', $request->file( 'image' ) );
            // $post_info->file_name       = basename( $file );
            $user_info->profile_photo      = Storage::disk( 's3' )->url( $file );
            $user_info->save();
        }
        return response()->json( [ 'success' => 'Profile image updated successfully.' ] );
    }

    
}
