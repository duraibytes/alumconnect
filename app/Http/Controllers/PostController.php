<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;

use App\Models\Category;
use App\Models\Post;

class PostController extends Controller
{

    function api_blog() {
        $post_info                      = Post::with( [ 'category_info' ] )
                                                ->where( [ 'type' => 'BLOG' ] )
                                                ->orderBy( 'post.title', 'Asc' )
                                                ->get();
        return $post_info;
    }

    function blog( Request $request ) { 
        
        $title                          = 'Post';
        $is[ 'is_drop_menu' ]           = 'no';
        $is[ 'tooltip' ]                = 'Add Blog';
        $is[ 'btn_link' ]               = 'return open_add_blog_form()';
        $is[ 'btn_name' ]               = 'Add Blog';
        
        $post_info                      = Post::with( [ 'category_info' ] )
                                                ->where( [ 'type' => 'BLOG' ] )
                                                ->orderBy( 'post.title', 'Asc' )
                                                ->get();
        // if( $post_info ) {
        //     foreach( $post_info as $info ) {
        //         return Storage::disk( 's3' )->response( 'post/'. $info->file_name );
        //     }
        // }

        return view( 'pages.post.index', [ 'title' => $title, 'action' => $is, 'post_info' => $post_info ] );
    }

    function add_blog_form( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $category                       = Category::where( [ 'type' => 'BLOG' ] )->get();
        $title                          = 'Blog';
        $type                           = 'BLOG';
        return view( 'pages.post.form.add', [ 'type' => $type, 'category' => $category, 'title' => $title ] );
    }

    function insert_post( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 

        
        if( $request->type == 'VIDEO' ) {
            $post_validator   = [
                'title'             => [ 'required', 'max:255' ],
                'category_id'       => [ 'required' ],
                'url'               => [ 'required', 'url' ]
            ];
        } else {
            $post_validator   = [
                'title'             => [ 'required', 'max:255' ],
                'category_id'       => [ 'required' ],
            ];
        }
        
        //Validate the product
        $validation                      = Validator::make( $request->all(), $post_validator );
        if( $validation->fails() ) {
            $error_msg                   = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }

        $type                           = $request->type;
        
        $post_info[ 'title' ]           = $request->title;
        $post_info[ 'category_id' ]     = $request->category_id ;
        $post_info[ 'created_by' ]      = $request->user()->id;
        $post_info[ 'status' ]          = 'DRAFT';
        $post_info[ 'type' ]            = $type;
        if( $type == 'VIDEO' ) {
            $post_info[ 'video_url' ]   = $request->url;
        }
       
        $post                           = Post::create( $post_info );
        $url                            = route( 'post.edit', [ 'id' => $post->id ] );
        return response()->json( [ 'success' => $type.'  is created successfully.', 'id' => $post->id, 'url' => $url ] );
    }

    function edit_post(  Request $request, $id ) {

        $post_info                      = Post::with([ 'category_info' ] )->find( $id );
        $tag_info                       = Category::Tag()->get();
        $title                          = 'Edit '. $post_info->category_info->type;
        $data[ 'login_name' ]           = $request->user()->name;
        $data[ 'login_email' ]          = $request->user()->email;
        $is[ 'is_menu' ]                = 'no';
        $type                           = $post_info->type;
            
        $category                       = Category::where( [ 'type' => $type ] )->get();
        return view( 'pages.post.form.edit_blog', [ 'type' => $type, 
                'category' => $category, 'title' => $title, 
                'user_info' => $data, 'action' => $is, 
                'post_info' => $post_info, 'id' => $id,
                'tag_info'  => $tag_info 
            ] );
    }

    function update_post( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 
        
        if( $request->type == 'BROADCAST' ) {
            $post_validator   = [
                'title'             => [ 'required', 'max:255' ],
                'category_id'       => [ 'required' ],
                'url'               => [ 'required', 'url' ]
            ];
        } else {
            $post_validator   = [
                'title'             => [ 'required', 'max:255' ],
                'category_id'       => [ 'required' ],
            ];
        }
       
        //Validate the product
        $validation                      = Validator::make( $request->all(), $post_validator );
        if( $validation->fails() ) {
            $error_msg                   = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }

        $id                             = $request->id;
        $type                           = $request->type;
        $post_info                      = Post::find( $id );
        $post_info->title               = $request->title;
        $post_info->category_id         = $request->category_id;
        $post_info->sub_category_id     = $request->sub_category_id;
        if( $type == 'BROADCAST' ){
            $post_info->video_url       = $request->url;
        }
        $post_info->status              = $request->status;
        $post_info->description         = $request->description;
        if( $request->hasFile( 'image' ) ) {
            
            $file                       = $request->file( 'image' )->store( 'post', 's3' );
            // set visibility for public
            Storage::disk( 's3' )->setVisibility( $file, 'public' );
            // $file                       = Storage::putFile( 'post', $request->file( 'image' ) );
            $post_info->file_name       = basename( $file );
            $post_info->post_image      = Storage::disk( 's3' )->url( $file );
        }
        $post_info->save();
        
        return response()->json( [ 'success' => 'Post  is updated successfully.', 'type' => $type ] );
    }

    function delete_post( Request $request ) {
        $id                             = $request->id;
        $post_info                      = Post::findOrFail( $id );
        $type                           = $post_info->type;
        $post_info->forceDelete();
        
        return response()->json( ['success' => 'Post is deleted successfully.', 'type' => $type ] );
    }

    // video fuction starts from herer

    function video( Request $request ) { 
        
        $title                          = 'Broadcast';
        $is[ 'is_drop_menu' ]           = 'no';
        $is[ 'tooltip' ]                = 'Add Broadcast';
        $is[ 'btn_link' ]               = 'return open_add_video_form()';
        $is[ 'btn_name' ]               = 'Add Broadcast';
        $post_info                      = Post::with( [ 'category_info' ] )
                                            ->where( [ 'type' => 'BROADCAST' ] )
                                            ->orderBy( 'post.title', 'Asc' )
                                            ->get();
        return view( 'pages.post.index', [ 'title' => $title, 'action' => $is, 'post_info' => $post_info ] );
    }

    function add_video_form( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $category                       = Category::where( [ 'type' => 'BROADCAST' ] )->get();
        $title                          = 'Broadcast';
        $type                           = 'BROADCAST';
        return view( 'pages.post.form.add', [ 'category' => $category, 'title' => $title, 'type' => $type ] );
    }
}
