<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApprovalConfirmationMail;

use App\Models\Category;
use App\Models\Student;
use DB;
use App\Exports\StudentExport;
use App\Models\Country;
use Maatwebsite\Excel\Facades\Excel;
use Config;

class StudentController extends Controller
{
    function index( Request $request ) { 
        
        $title                          = 'Students';
        $is[ 'is_menu' ]                = 'no';
        $countries_list                 = Country::get();
        $user_types                     = Category::where( 'type', 'STUDENT' )->get();
        $qualifications                 = Category::where( 'type', 'QUALIFICATIONS' )->get();
        $study_area                     = Category::where( 'type', 'STUDYAREA' )->get();
        $referral_status                = Category::where( 'type', 'REFERRAL_STATUS' )->get();
        
    //    dd( config( 'constants.LANGUAGES' ) );
        return view( 'pages.student.index', [ 'title' => $title, 
        'action' => $is, 
        'countries_list' => $countries_list, 
        'user_types' => $user_types, 
        'qualifications' => $qualifications, 
        'study_area' => $study_area, 
        'referral_status' => $referral_status 
        ] 
    );
    }

    function get_students( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }

        $columns                    = [ 'students.id', 'students.name','students.category', 'students.created_at', 'students.referred_by_user_id', 'students.country_id', '' ];

        $limit                      = $request->input( 'length' );
        $start                      = $request->input( 'start' );
        $order                      = $columns[ intval( $request->input( 'order' )[0][ 'column' ] ) ];        
        $dir                        = $request->input( 'order' )[0][ 'dir' ];
        $search                     = $request->input( 'search' );
        $filter_user_type           = $request->input( 'filter_user_type' );
        $filter_country             = $request->input( 'filter_country' );
        $filter_study_area          = $request->input( 'filter_study_area' );
        $filter_language            = $request->input( 'filter_language' );
        $filter_intake              = $request->input( 'filter_intake' );
        $filter_referral_status     = $request->input( 'filter_referral_status' );
        

        $total_users      = Student::with( [ 'category_info', 'country_info' ] )
                            ->OnlyStudent()
                            ->JoinCountry()
                            ->count();

        $users            = Student::with( [ 'category_info', 'country_info' ] )
                            ->OnlyStudent()
                            ->JoinCountry()
                            ->search( $search )
                            ->offset( $start )
                            ->limit( $limit )
                            ->orderBy( $order, $dir )
                            ->select( 'students.*', 'countries.name as country_name' );
        if( !empty( $filter_user_type ) ) {
            $users          =  $users->where( 'category', $filter_user_type );
        }
        if( !empty( $filter_country ) ) {
            $users          =  $users->where( 'country_id', $filter_country );
        }
        if( !empty( $filter_study_area ) ) {
            $users          =  $users->where( 'study_area', $filter_study_area );
        }
        if( !empty( $filter_language ) ) {
            $users          =  $users->where( 'language', $filter_language );
        }
        if( !empty( $filter_intake ) ) {
            $users          =  $users->where( 'intake', $filter_intake );
        }
        if( !empty( $filter_referral_status ) ) {
            $users          =  $users->where( 'referral_status', $filter_referral_status );
        }
        $users              =  $users->get();
        
        $total_filtered = Student::with( [ 'category_info', 'country_info' ] )
                                ->OnlyStudent()
                                ->JoinCountry()
                                ->orderBy( $order, $dir );

        if( !empty( $search ) ) {
            $total_filtered =  $total_filtered->search( $search );
        }
        if( !empty( $filter_user_type ) ) {
            $total_filtered =  $total_filtered->where( 'category', $filter_user_type );
        }
        if( !empty( $filter_country ) ) {
            $users          =  $users->where( 'country_id', $filter_country );
        }
        if( !empty( $filter_study_area ) ) {
            $users          =  $users->where( 'study_area', $filter_study_area );
        }
        if( !empty( $filter_language ) ) {
            $users          =  $users->where( 'language', $filter_language );
        }
        if( !empty( $filter_intake ) ) {
            $users          =  $users->where( 'intake', $filter_intake );
        }
        if( !empty( $filter_referral_status ) ) {
            $users          =  $users->where( 'referral_status', $filter_referral_status );
        }
        $total_filtered     =  $total_filtered->count();
        
        $data                       = array();
        
        if( $users ) {
            
            foreach( $users as $user ) {
                
                $user_status                        = '';
                $user_category                      = '';
                $country                            = '';
                if( !empty( $user->country_info->name ) ) {
                    $country                        = '<img class="w-20px" src="'. asset( 'images/icon-flag/'. strtolower( $user->country_info->iso ).'.svg' ).'" >';
                }
                $user_info                          = '<span style="width: 250px;">
                <div class="d-flex align-items-center">
                   <div class="symbol symbol-50 symbol-sm flex-shrink-0">
                        <div class="symbol-label">                                
                          <img class="w-20px" src="https://preview.keenthemes.com/metronic/demo2/custom/apps/contacts/assets/media/svg/avatars/048-boy-21.svg" alt="photo">                            
                        </div>
                   </div>
                   <div class="ml-4">
                      <div class="text-dark-75 font-weight-bolder font-size-lg mb-0" onclick="student_info( ' . $user->id . ' );">
                      '.ucwords( $user->name ).' '. $user->fname.'
                      </div>
                      <a href="#" class="text-muted font-weight-bold text-hover-primary">'.$user->email.'</a>                        
                   </div>
                   <div class="symbol-label pl-5">                                
                       '.$country.'
                    </div>
                </div>
             </span>';

                $class                              = 'badge-warning';
                $checked                            = '';
                $approve_stu                        = '';
                $disabled                           = 'disabled readonly';
                if( $user->category_info ) {
                    if( $user->category_info->name == 'FUTURE' ) {
                        $disabled                   = '';
                    }
                    $user_category                 .= '<span class="badge badge-primary">'.$user->category_info->name.'</span>';
                } 
                $last_login                         = '';
                if( $user->last_login ) {
                    $last_login                     = date( 'd/m/Y H:i a', strtotime( $user->last_login ) );
                }
                if( $user->status == 1 ) {
                    $status_label                   = 'Approved';
                    $status_btn                     = '<span class="label label-md font-weight-bold  label-light-success label-inline"> Approved </span>';
                    $class                          = 'badge-success';
                    $approve_stu                    = 'switch-success switch-outline';
                    $checked                        = 'checked';
                    $approve_status                 = 2;
                } else if( $user->status == 0 ) {
                    $status_btn                     = '<span class="label label-md font-weight-bold  label-light-info label-inline"> New </span>';
                    $status_label                   = 'New';
                    $approve_status                 = 1;
                } else {
                    $status_label                   = 'DisApproved';
                    $status_btn                     = '<span class="label label-md font-weight-bold  label-light-danger label-inline"> Dis Approved </span>';
                    $approve_status                 = 1;
                }
                $refered                            = '<div class="text-center" ><i class="flaticon2-cross text-danger"></i></div>';
                if( $user->referred_by_user_id != 0 ) {
                    $refered                        = '<div class="text-center" ><i class="flaticon2-checkmark text-success"></i></div>';
                }
                
                $edit_btn                           = '<span class="switch switch-icon '.$approve_stu.'" >
                                                            <label>
                                                                <input type="checkbox" '.$checked.' name="select" '.$disabled.'onclick="return are_you_sure( \'approve_student_status\', \'Change Student Status\', \'Are you sure want to change status?\', ' . $user->id . ', \''.$approve_status.'\' )">
                                                                <span></span>
                                                            </label>
                                                        </span>';
                                                        
                $nested_data[ '#' ]                 = $user->id;
                $nested_data[ 'users' ]             = $user_info;
                $nested_data[ 'category' ]          = $user_category;
                $nested_data[ 'joinedon' ]          = date( 'd/m/Y  G:i a', strtotime( $user->created_at ) );
                
                $nested_data[ 'status' ]            = $status_btn;
                $nested_data[ 'actions' ]           = $edit_btn;
                
                $data[]                             = $nested_data;

            }
        }

        return response()->json( [ 
            'draw'              => intval( $request->input( 'draw' ) ),
            'recordsTotal'      => intval( $total_users ),
            'data'              => $data,
            'recordsFiltered'   => intval( $total_filtered )
        ] );
    }

    function approve( Request $request ) {
        $id                             = $request->id;
        $status                         = $request->status;
        $student_info                   = Student::findOrFail( $id );
        $student_info->status           = $status;
        $student_info->save();      
        $student_email                  = $student_info->email;
        //APPROVE MAIL
        if( $status == 1 ) {
            Mail::to( $student_email )->send( new ApprovalConfirmationMail( $student_info ) );
        }
        
        return response()->json( ['success' => 'Student status changed successfully.' ] );
    }

    public function export() 
    {
        return Excel::download( new StudentExport, 'students.xlsx' );
    }

    function referrals( Request $request ) { 
        
        $title                          = 'Referrals';
        $is[ 'is_menu' ]                = 'no';
        
        return view( 'pages.referrals.index', [ 'title' => $title, 'action' => $is ] );
    }

    // referrals starts from here
    function get_referrals( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }

        $columns            = [ 'students.id', 'students.name','students.category', 'students.created_at', 'students.referred_by_user_id', 'students.country_id', '' ];

        $limit              = $request->input( 'length' );
        $start              = $request->input( 'start' );
        $order              = $columns[ intval( $request->input( 'order' )[0][ 'column' ] ) ];        
        $dir                = $request->input( 'order' )[0][ 'dir' ];
        $search             = $request->input( 'search.value' );
        

        $total_referred      = Student::with( [ 'category_info', 'country_info', 'referred_info' ] )
                            ->OnlyStudent()
                            ->JoinReferred()
                            ->JoinCountry()
                            ->count();
        

        $referred            = Student::with( [ 'category_info', 'country_info', 'referred_info'  ] )
                            ->OnlyStudent()
                            ->JoinReferred()
                            ->JoinCountry()
                            ->search( $search )
                            ->offset( $start )
                            ->limit( $limit )
                            ->orderBy( $order, $dir )
                            ->select( 'students.*', 'countries.name as country_name' )
                            ->get();

        

        if( empty( $request->input( 'search.value' ) ) ) {
            $total_filtered = Student::with( [ 'category_info', 'country_info', 'referred_info'  ] )
                                ->OnlyStudent()
                                ->JoinReferred()
                                ->JoinCountry()
                                ->orderBy( $order, $dir )
                                
                                ->count();
        } else {
            $total_filtered =  Student::with( [ 'category_info', 'country_info', 'referred_info'  ] )
                                ->OnlyStudent()
                                ->JoinReferred()
                                ->JoinCountry()
                                ->search( $search )
                                ->orderBy( $order, $dir )
                                ->count();
        }
        
        $data                       = array();
        
        if( $referred ) {
            
            foreach( $referred as $ref ) {
                
                $user_status                        = '';
                $user_category                      = '';
                $user_info                          = '<span style="width: 250px;">
                <div class="d-flex align-items-center">
                   <div class="symbol symbol-50 symbol-sm flex-shrink-0">
                      <div class="symbol-label">                                
                          <img class="w-25px" src="https://preview.keenthemes.com/metronic/demo2/custom/apps/contacts/assets/media/svg/avatars/048-boy-21.svg" alt="photo">                            </div>
                   </div>
                   <div class="ml-4">
                      <div class="text-dark-75 font-weight-bolder font-size-lg mb-0">'.ucwords( $ref->name ).' '. $ref->fname.'</div>
                      <a href="#" class="text-muted font-weight-bold text-hover-primary">'.$ref->email.'</a>   
                      ' . ( ( $ref->country_info ) ? '<div class="text-primary font-weight-bold">'.  $ref->country_info->name.'</div>' : '' ) . '                     
                   </div>
                </div>
             </span>';

             $refer_info                          = '<span style="width: 250px;">
                <div class="d-flex align-items-center">
                   <div class="symbol symbol-50 symbol-sm flex-shrink-0">
                      <div class="symbol-label">                                
                          <img class="w-25px" src="https://preview.keenthemes.com/metronic/demo2/custom/apps/contacts/assets/media/svg/avatars/048-boy-21.svg" alt="photo">                            </div>
                   </div>
                   <div class="ml-4">
                      <div class="text-dark-75 font-weight-bolder font-size-lg mb-0">'.ucwords( $ref->referred_info->name ).' '. $ref->referred_info->fname.'</div>
                      <a href="#" class="text-muted font-weight-bold text-hover-primary">'.$ref->referred_info->email.'</a> 
                      <div class="text-primary font-weight-bold">'. $ref->referred_info->country.'</div>                       
                   </div>
                </div>
             </span>';
                if( $ref->category_info ) {
                    
                    $user_category                 .= '<span class="badge badge-primary">'.$ref->category_info->name.'</span>';
                } 
                $last_login                         = '';
                if( $ref->last_login ) {
                    $last_login                     = date( 'd/m/Y G:i a', strtotime( $ref->last_login ) );
                }
                if( $ref->status == 1 ) {
                    $status_label                   = 'Approved';
                    $status_btn                     = '<a onClick="return are_you_sure( \'approve_student\', \'Approve student\', \'Are you sure want to approve?\', ' . $ref->id . ', \'2\' )" href="javascript:;" class="navi-link"> Disapprove </a>';
                } else {
                    $status_label                   = 'DisApproved';
                    $status_btn                     = '<a onClick="return are_you_sure( \'approve_student\', \'Approve student\', \'Are you sure want to approve?\', ' . $ref->id . ', \'1\' )" href="javascript:;" class="navi-link"> Approve </a>';
                }
                $refered                            = '<div class="text-center" ><i class="flaticon2-cross text-danger"></i></div>';
                if( $ref->referred_by_user_id != 0 ) {
                    $refered                        = '<div class="text-center" ><i class="flaticon2-checkmark text-success"></i></div>';
                }
                $succes_pay                         = '';
                $checked                            = '';
                $disabled                           = 'disabled';
                $class                              = 'badge-warning';
                $change_payment_status              = 1;     
                if( $ref->referral_status == 'REFERRED' ) {
                    $class                          = 'badge-success';
                    $disabled                       = 'disabled';
                } else if ( $ref->referral_status == 'APPLIED' ) {
                    $disabled                       = '';
                    if( $ref->referral_payment_status ) {
                        $succes_pay                 = 'switch-success switch-outline';
                        $checked                    = 'checked';
                        $disabled                   = '';
                        $change_payment_status      = 0;
                    }
                }
                
                $edit_btn                           = '<span class="switch switch-icon '.$succes_pay.'" >
                                                            <label>
                                                                <input type="checkbox" '.$checked.' name="select" '.$disabled.' onclick="return are_you_sure( \'change_payment_status\', \'Change Payment Status\', \'Are you sure want to change payment status?\', ' . $ref->id . ', \''.$change_payment_status.'\' )">
                                                                <span></span>
                                                            </label>
                                                        </span>';
                
                $status                             = '<span ' . ( ( $ref->category == 1 ) ? 'onclick="change_referral_status( ' . $ref->id . ' );" style="cursor: pointer;"' : '' ) . ' class="badge '.$class.'">'. str_replace( '_', ' ', $ref->referral_status ) .'</span>';
                                                        
                $nested_data[ '#' ]                 = $ref->id;
                $nested_data[ 'students' ]          = $user_info;
                $nested_data[ 'category' ]          = $user_category;
                $nested_data[ 'referredon' ]        = date( 'd/m/Y  G:i a', strtotime( $ref->created_at ) );
                $nested_data[ 'referredby' ]        = $refer_info;
                $nested_data[ 'status' ]            = $status;
                $nested_data[ 'paid' ]              = $edit_btn;
                
                $data[]                             = $nested_data;

            }
        }

        return response()->json( [ 
            'draw'              => intval( $request->input( 'draw' ) ),
            'recordsTotal'      => intval( $total_referred ),
            'data'              => $data,
            'recordsFiltered'   => intval( $total_filtered )
        ] );
    }

    function change_payment_status( Request $request ) {
        
        $id                                             = $request->id;
        $status                                         = $request->status;
        $student_info                                   = Student::findOrFail( $id );
        $student_info->referral_payment_status          = $status;
        $student_info->save();                   
        
        return response()->json( ['success' => 'Payment status changed successfully.' ] );

    }

    function modal_info( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $student_id                     = $request->student_id;
        $student_info                   = Student::with( [ 'category_info', 'country_info' ] )
                                            ->find( $student_id );
        $title                          = 'Student Info';

        return view( 'pages.student.info', [ 'student_info' => $student_info, 'title' => $title, 'id' => $student_id ] );
    }

    function change_referral_status( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $student_id                     = $request->student_id;
        $student_info                   = Student::find( $student_id );
        $title                          = 'Change Referral Status';
        $referral_status                = Category::where( 'type', 'REFERRAL_STATUS' )->get();
        
        return view( 'pages.referrals.change_referral_status', [ 'student_info' => $student_info, 'title' => $title, 'id' => $student_id, 'referral_status' => $referral_status ] );
    }

    function update_referral_status( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 
        
        $referral_status_validator          =   [
                                                    'referral_status'             => [ 'required' ],
                                                ];
        //Validate the product
        $validation                         = Validator::make( $request->all(), $referral_status_validator );
        if( $validation->fails() ) {
            $error_msg                      = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }

        $id                                 = $request->id;
        
        $student_info                       = Student::find( $id );
        $student_info->referral_status      = $request->referral_status;
        $student_info->save();
        
        return response()->json( [ 'success' => 'Student referral status is updated successfully.' ] );
    }

    function approve_student_status( Request $request ) {
        $id                                             = $request->id;
        $status                                         = $request->status;
        $student_info                                   = Student::findOrFail( $id );
        $student_info->status                           = $status;
        $student_info->save();  
        $msg                                            = 'Student has been disapproved';
        if( $status == 1 ) {
            $msg                                        = 'Student has been approved';
        }
        
        return response()->json( [ 'success' => $msg ] );
    }
}
