<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordMail;

class UserController extends Controller
{

    function index( Request $request ) { 
        $user_list                      = User::get();
        $title                          = 'Users';
        $is[ 'is_drop_menu' ]           = 'no';
        $is[ 'tooltip' ]                = 'Add User';
        $is[ 'btn_link' ]               = 'return open_add_user_form()';
        $is[ 'btn_name' ]               = 'Add User';
        return view( 'pages.user.index', [ 'user_list' => $user_list, 'title' => $title,  'action' => $is ] );
    }

    function add_user_form( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $category                       = Category::where( [ 'type' => 'USER' ] )->get();
        
        return view( 'pages.user.form.add', [ 'category' => $category ] );
    }

    function insert_user( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 

        $user_validator   = [
            'fname'             => [ 'required', 'max:255' ],
            'name'              => [ 'required' ],
            'email'             => [ 'required', 'unique:users', 'email' ],
            'status'            => [ 'required' ],
            
        ];
        //Validate the product
        $validation                      = Validator::make( $request->all(), $user_validator );
        if( $validation->fails() ) {
            $error_msg                   = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }

        //if pass then insert from here
        $bytes                          = random_bytes( 4 );
        $gen_password                   = bin2hex($bytes);

        $password                       = Hash::make( $gen_password );

        $user_data[ 'fname' ]           = $request->fname;
        $user_data[ 'name' ]            = $request->name;
        $user_data[ 'category' ]        = $request->category;
        $user_data[ 'email' ]           = $request->email;
        $user_data[ 'status' ]          = $request->status;
        $user_data[ 'password' ]        = $password;
        $user_data[ 'type' ]            = 'A';

        $user                           = User::create( $user_data );
        if( $user->status == 1 ) {
            Mail::to( $user->email )->send( new PasswordMail( $gen_password ) ); 
        }
        
        return response()->json( [ 'success' => 'User is created successfully.' ] );
    }

    function get_users( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
 
        $columns            = [ 'users.id', 'users.name', 'users.category', 'last_login', '' ];

        $limit              = $request->input( 'length' );
        $start              = $request->input( 'start' );
        $order              = $columns[ intval( $request->input( 'order' )[0][ 'column' ] ) ];        
        $dir                = $request->input( 'order' )[0][ 'dir' ];
        $search             = $request->input( 'search.value' );
        

        $total_users      = User::with( [ 'category_info' ] )
                            ->OnlyAdmin()
                            ->count();

        $users            = User::with( [ 'category_info' ] )
                            ->OnlyAdmin()
                            ->search( $search )
                            ->offset( $start )
                            ->limit( $limit )
                            ->orderBy( $order, $dir )
                            ->get();

        if( empty( $request->input( 'search.value' ) ) ) {
            $total_filtered = User::with( [ 'category_info' ] )
                                ->OnlyAdmin()
                                ->orderBy( $order, $dir )
                                ->count();
        } else {
            $total_filtered =  User::with( [ 'category_info' ] )
                                ->OnlyAdmin()
                                ->search( $search )
                                ->orderBy( $order, $dir )
                                ->count();
        }
        
        $data                       = array();
        if( $users ) {
            $i                                      = 1;
            foreach( $users as $user ) {
                
                $user_status                        = '';
                $user_category                      = '';
                $user_info                          = '<span style="width: 250px;">
                <div class="d-flex align-items-center">
                   
                   <div class="ml-4">
                      <div class="text-dark-75 font-weight-bolder font-size-lg mb-0">'.ucwords( $user->name ).' '. $user->fname.'</div>
                      <a href="#" class="text-muted font-weight-bold text-hover-primary">'.$user->email.'</a>                        
                   </div>
                </div>
             </span>';
                if( $user->category_info ) {
                    
                    $user_category                 .= '<span class="badge badge-primary">'.$user->category_info->name.'</span>';
                    
                } 
                $last_login                         = '';
                if( $user->last_login ) {
                    $last_login                     = date( 'd/m/Y G:i a', strtotime( $user->last_login ) );
                }

                
                $edit_btn                           = '&emsp;&emsp;&emsp;<a href="javascript:;" onClick="return edit_user( ' . $user->id . ' )" class="btn btn-sm btn-warning btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">
                <span class="svg-icon svg-icon-md">
                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                         <rect x="0" y="0" width="24" height="24"></rect>
                         <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "></path>
                         <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                      </g>
                   </svg>
                </span>
             </a>
             ';
                
                $nested_data[ '#' ]                 = $user->id;
                $nested_data[ 'users' ]             = $user_info;
                $nested_data[ 'category' ]          = $user_category;
                $nested_data[ 'lastloggedin' ]      = $last_login;
                $nested_data[ 'actions' ]           = $edit_btn;
                
                $data[]                             = $nested_data;
                $i++;
            }
        }

        return response()->json( [ 
            'draw'              => intval( $request->input( 'draw' ) ),
            'recordsTotal'      => intval( $total_users ),
            'data'              => $data,
            'recordsFiltered'   => intval( $total_filtered )
        ] );
    }

    function edit_user_form( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        }
        $category                       = Category::where( [ 'type' => 'USER' ] )->get();
        $id                             = $request->id;
        $user_info                      = User::find( $id );
        
        return view( 'pages.user.form.edit', [ 'category' => $category, 'user_info' => $user_info, 'id' => $id ] );
    }

    function update_user( Request $request ) {
        if (! $request->ajax()) {
            return response('Forbidden.', 403);
        } 
        $user_validator   = [
            'fname'             => [ 'required', 'max:255' ],
            'name'              => [ 'required' ],
            'status'            => [ 'required' ],
            
        ];
        //Validate the product
        $validation                      = Validator::make( $request->all(), $user_validator );
        if( $validation->fails() ) {
            $error_msg                   = $validation->errors()->all();
            return response()->json( [ 'failure' => $error_msg ] );
        }
        $id                             = $request->id;

        
        $user_info                      = User::find( $id );
        
        if( $user_info->status != 1 && $request->status == 1 ) {
            //if pass then insert from here
            $bytes                      = random_bytes( 4 );
            $gen_password               = bin2hex($bytes);

            $password                   = Hash::make( $gen_password );
            $user_info->password        = $password;
        }
        $user_info->fname               = $request->fname;
        $user_info->name                = $request->name;
        $user_info->category            = $request->category;
        $user_info->status              = $request->status;
        $user_info->save();

        if( isset( $gen_password ) ) {
            Mail::to( $user_info->email )->send( new PasswordMail( $gen_password ) ); 
        }
        return response()->json( [ 'success' => 'User is updated successfully.' ] );
    }

    
}
