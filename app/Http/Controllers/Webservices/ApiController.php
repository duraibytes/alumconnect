<?php

namespace App\Http\Controllers\Webservices;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;

use App\Models\Category;
use App\Models\Post;
use App\Models\Student;
use App\Models\Country;

use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordMail;
use App\Mail\ApprovalRequestMail;
use App\Mail\ReferralMail;
use App\Mail\ChangeEmailMail;
use App\Mail\FeedbackMail;

class ApiController extends Controller
{
    function featured_blog( Request $request, $limit = 3, $type = 'BLOG' ) { 
        $api_data                      	= Post::with( [ 'category_info' ] )
                                                ->where( [ 'post.type' => $type ] )
                                                ->where( [ 'post.is_featured' => 1 ] )
                                                ->where( [ 'post.status' => 'PUBLISHED' ] )
                                                ->orderBy( 'post.id', 'Desc' )
                                                ->limit( $limit )
                                                ->get();
        return $api_data;
    }

    function blog_list( Request $request, $type = 'BLOG', $category_id = '' ) { 
        
        $api_data                      	= Post::with( [ 'category_info' ] )
        										->where( [ 'post.type' => $type] )
        										->where( [ 'post.status' => 'PUBLISHED' ] )
                                                ->orderBy( 'post.title', 'Asc' );
        if( !empty( $category_id ) ) {
        	$api_data               	= $api_data->where( [ 'post.category_id' => $category_id] );
        }                   
        $api_data                      	= $api_data->get();
        return $api_data;
    }

    function blog_info( Request $request, $id ) { 
        $api_data                      	= Post::with( [ 'category_info' ] )
        										->where( [ 'id' => $id] )
                                                ->get();
        return $api_data;
    }

    function category_list( Request $request, $type = 'BLOG', $tag = 'COMMUNITY' ) { 
        $api_data                      	= Category::with( [ 'subcategory_list' ] )
        										->where( [ 'categories.type' => $type] )
        										->where( [ 'categories.page' => $tag] )
        										->where( [ 'categories.parent_id' => 0] )
                                                ->orderBy( 'categories.name', 'Asc' )
                                                ->get();
        return $api_data;
    }

    function subcategory_list( Request $request, $category_id, $type = 'TAG', $tag = '' ) { 
        $api_data                      	= Category::where( [ 'categories.type' => $type] )
        										->where( [ 'categories.parent_id' => $category_id] )
        										->where( 'categories.parent_id', '<>', 0 )
                                                ->orderBy( 'categories.name', 'Asc' )
                                                ->get();
        return $api_data;
    }

    function community( Request $request, $category_id = '', $limit = 3, $type = 'BLOG', $tag = 'COMMUNITY' ) { 
        $array[ 'featured_blog' ]			= $this->featured_blog( $request, $limit, $type );
        
        $array[ 'category_list' ]    		= $this->category_list( $request, $type, $tag );
        if( empty( $category_id ) ) {
        	$category_id 					= $array[ 'category_list' ][0]->id;
        }

        $category_list_array 				= (array) $array[ 'category_list' ];
        $key 								= array_search( $category_id, array_column( $category_list_array, 'id' ) );
        $array[ 'category_info' ]  			= $array[ 'category_list' ][$key];

        if( !empty( $category_id ) ) {
        	$array[ 'subcategory_list' ]  	= $this->subcategory_list( $request, $category_id, 'TAG', '' );
        }

        $array[ 'blog_list' ]      			= $this->blog_list( $request, $type, $category_id );

		return $array;
    }

    function my_univ( Request $request, $category_id = '', $limit = 3, $type = 'BLOG', $tag = 'MYUNI' ) { 
        $array[ 'featured_blog' ]           = $this->featured_blog( $request, $limit, $type );
        
        $array[ 'category_list' ]           = $this->category_list( $request, $type, $tag );
        if( empty( $category_id ) ) {
            $category_id                    = $array[ 'category_list' ][0]->id;
        }

        $category_list_array                = (array) $array[ 'category_list' ];
        $key                                = array_search( $category_id, array_column( $category_list_array, 'id' ) );
        $array[ 'category_info' ]           = $array[ 'category_list' ][$key];

        if( !empty( $category_id ) ) {
            $array[ 'subcategory_list' ]    = $this->subcategory_list( $request, $category_id, 'TAG', '' );
        }

        $array[ 'blog_list' ]               = $this->blog_list( $request, $type, $category_id );

        return $array;
    }

    function check_student_email( Request $request ) { 
        
    	$return_data[ 'status' ] 	        = false;
    	if( $request->has( [ 'email' ] ) ) {
	    	$student_email 					= $request->input( 'email' ); //'husein@thecodingoven.com';
	        $api_data                      	= Student::select('id')->where( [ 'email' => $student_email ] )
	                                                ->get()->first();	     
	        if( !empty( $api_data ) ) {
	        	$student_id 				= $api_data->id;
	        	$otp 						= get_rand_numbers(5); //get_rand_letters(1)
	        	$otp_date 					= date( 'Y-m-d H:i:s' );
	        	//UPDATE OTP
	        	Student::where( 'id', $student_id )->update( [ 'otp'=> $otp, 'otp_date'=> $otp_date ] );
                Mail::to( $student_email )->send( new PasswordMail( $otp, 'otp' ) );
	        	$return_data[ 'status' ] 	= true;
	        }
            
	    }
       

	    return $return_data;
    }

    function authenticate_otp( Request $request ) { 

    	$return_data[ 'status' ] 	            = false;
    	if( $request->has( [ 'email' ] ) && $request->has( [ 'otp' ] ) ) {
            $student_email 					    = $request->input( 'email' ); //'husein@thecodingoven.com';
	    	$student_otp 					    = $request->input( 'otp' ); //'c8799';
	        $student_data                       = Student::with( [ 'category_info' ] )->where( [ 'otp' => $student_otp, 'email' => $student_email ] )
	                                                ->get()->first();	     
	        if( !empty( $student_data ) ) {
	        	//CREATE TOKEN
	        	$token 						    = $student_data->createToken( $student_email );
	        	//print_r( $token );
                $plainTextToken                 = $token->plainTextToken;
	        	$return_data[ 'status' ] 	    = true;
                $return_data[ 'token' ] 	    = $plainTextToken;
                $return_data[ 'student_info' ] 	= $student_data;
	        }
	    }

	    return $return_data;
    }

    function create_student( Request $request ) { 

    	$return_data[ 'status' ] 	            = false;
    	if( $request->has( [ 'email' ] ) && $request->has( [ 'referral_code' ] ) ) {
            $email 					            = $request->input( 'email' );
	    	$referral_code 				        = $request->input( 'referral_code' );
            $category 		                    = $request->input( 'category' );
            $student_info                       = Student::where( [ 'email' => $email ] )->where('referral_status', '<>' , 'REFERRED')
	                                                ->get()->first();
            $referral_student_info              = Student::where( [ 'referral_code' => $referral_code ] )
	                                                ->get()->first();
            $student_id_file                    = '';
            $student_id_s3                      = '';
            if( $category == 3 ) {
                if( $request->hasFile( 'student_id_file' ) ) {
                    $file                       = $request->file( 'student_id_file' )->store( 'student_id', 's3' );
                    Storage::disk( 's3' )->setVisibility( $file, 'public' );
                    $student_id_file            = basename( $file );
                    $student_id_s3              = Storage::disk( 's3' )->url( $file );
                }
            }

            if( empty( $student_info ) && !empty( $referral_student_info ) ) {
                $type 		                    = 'S';
                $fname 		                    = $request->input( 'fname' );
                $lname 		                    = $request->input( 'lname' );
                $name                           = $fname . ' ' . $lname;
                $password 					    = $request->input( 'password' );
                $mobile 					    = $request->input( 'mobile' );
                $random_referral_code 		    = get_rand_alphanumeric(8);
                $referred_by_user_id 		    = $referral_student_info->id;
                $referral_date                  = date( 'Y-m-d' );
                $referral_status                = 'APPLIED';
                $status                         = 0;
                $created_at                     = date( 'Y-m-d H:i:s' );

                $gen_password                   = bin2hex( $password );
                $password                       = Hash::make( $gen_password );

                $student_data                   = [ 
                                                    'type'                  => $type, 
                                                    'category'              => $category,
                                                    'name'                  => $name,
                                                    'fname'                 => $fname,
                                                    'lname'                 => $lname,
                                                    'mobile'                => $mobile,
                                                    'email'                 => $email,
                                                    'password'              => $password,
                                                    'referred_by_user_id'   => $referred_by_user_id,
                                                    'referral_date'         => $referral_date,
                                                    'referral_status'       => $referral_status,
                                                    'status'                => $status,
                                                    'created_at'            => $created_at,
                                                    'referral_code'         => $random_referral_code,
                                                    'student_id_file'       => $student_id_file,
                                                    'student_id_s3'         => $student_id_s3
                                                    ];
                //INSERT STUDENT
                $student                        = Student::create( $student_data );
	        	$return_data[ 'status' ] 	    = true;
                $return_data[ 'student_id' ]    = $student->id;
                //MAIL
                $student_info                   = Student::with( [ 'category_info', 'country_info' ] )
                                                    ->find( $student->id );
                Mail::to( SUPPORT_EMAIL )->send( new ApprovalRequestMail( $student_info ) );
	        } else {
                if( !empty( $student_info ) && empty( $referral_student_info ) ) {
                    $return_data[ 'error_msg' ] = 'Email ID already exist and Invalid Referral Code';
                } else if( !empty( $student_info ) ) {
                    $return_data[ 'error_msg' ] = 'Email ID already exist';
                } else if( empty( $referral_student_info ) ) {
                    $return_data[ 'error_msg' ] = 'Invalid Referral Code';
                }
            }
	    } else {
            $return_data[ 'error_msg' ] 	    = 'Email ID and Referral Code is required';
        }

	    return $return_data;
    }

    function student_avatar_upload( Request $request ) {
        
        if( $request->hasFile( 'student_avatar' ) && $request->has( 'student_id' ) ) {

            $student_id 			    = $request->input( 'student_id' );
            $file                       = $request->file( 'student_avatar' )->store( 'student_avatar', 's3' );
            Storage::disk( 's3' )->setVisibility( $file, 'public' );
            $avatar_file                = basename( $file );
            $avatar_s3                  = Storage::disk( 's3' )->url( $file );
            //UPDATE STUDENT
            Student::where( 'id', $student_id )->update( [ 'avatar_s3'=> $avatar_s3 ] );

            $return_data[ 'status' ] 	            = true;
            $return_data[ 'avatar_s3' ] 	        = $avatar_s3;
        } else {
            $return_data[ 'status' ] 	            = false;
        }

        return $return_data;
    }

    function referral_student_insert( Request $request ) {
        $return_data[ 'status' ] 	    = false;
        $referred_by_user_id            = $request->input( 'student_id' );
        $email                          = $request->input( 'email' );
        $referral_status                = 'REFERRED';
        $type                           = 'S';
        $referral_date                  = date( 'Y-m-d' );

        $student_info                   = Student::where( [ 'email' => $email ] )
	                                                ->get()->first();
        $referral_student_info          = Student::where( [ 'id' => $referred_by_user_id ] )
	                                                ->get()->first();
        if( !empty( $referral_student_info ) ) {
            $referral_code              = $referral_student_info->referral_code;
            $referral_name              = $referral_student_info->name;
        }

        if( empty( $student_info ) && !empty( $referral_student_info ) ) {
            $student_data                   = [ 
                                                'type'                  => $type, 
                                                'email'                 => $email,
                                                'referred_by_user_id'   => $referred_by_user_id,
                                                'referral_date'         => $referral_date,
                                                'referral_status'       => $referral_status,
                                                ];
            //INSERT STUDENT
            $student                        = Student::create( $student_data );
            $return_data[ 'status' ] 	    = true;
            $return_data[ 'student_id' ]    = $student->id;
            $return_data[ 'referral_code' ] = $referral_code;
            $return_data[ 'referral_name' ] = $referral_name;
            //MAIL
            Mail::to( $email )->send( new ReferralMail( $return_data ) );
        } else {
            if( !empty( $student_info ) && empty( $referral_student_info ) ) {
                $return_data[ 'error_msg' ] = 'Email ID already exist and Invalid Referrer';
            } else if( !empty( $student_info ) ) {
                $return_data[ 'error_msg' ] = 'Email ID already exist';
            } else if( empty( $referral_student_info ) ) {
                $return_data[ 'error_msg' ] = 'Invalid Referrer';
            }
        }

        return $return_data;
    }

    function change_profile_type( Request $request ) {
        $return_data[ 'status' ] 	            = false;
        if( $request->has( 'category' ) && $request->has( 'student_id' ) ) {
            $student_id                         = $request->input( 'student_id' );
            $category                           = $request->input( 'category' );
            $student_id_file                    = '';
            $student_id_s3                      = '';
            $student_info                       = Student::where( [ 'id' => $student_id ] )
	                                                ->get()->first();
            if( !empty( $student_info ) ) {
                $prev_category                  = $student_info->category;
                $status                         = $student_info->status;
                if( $prev_category != 3 && $category == 3 ) {
                    $status                     = 2;
                    if( $request->hasFile( 'student_id_file' ) ) {
                        $file                       = $request->file( 'student_id_file' )->store( 'student_id', 's3' );
                        Storage::disk( 's3' )->setVisibility( $file, 'public' );
                        $student_id_file            = basename( $file );
                        $student_id_s3              = Storage::disk( 's3' )->url( $file );
                    }
                }
                //UPDATE STUDENT
                Student::where( 'id', $student_id )->update( [ 'category'=> $category, 'status'=> $status, 'student_id_file' => $student_id_file, 'student_id_s3' => $student_id_s3 ] );

                $return_data[ 'status' ] 	    = true;
            } else {
                $return_data[ 'error_msg' ]     = 'Invalid student';
            }
        } else {
            $return_data[ 'error_msg' ]         = 'Student id and user type is required';
        }

        return $return_data;
    }

    function get_category_type_list( Request $request ) {

        //, 'referral_status' => 'REFERRAL_STATUS'
        $type_list                      = array( 'user_type' => 'STUDENT', 'qualifications' => 'QUALIFICATIONS', 'study_area' => 'STUDYAREA' );
        foreach( $type_list as $key => $type ) {
            $type_array[ $key ]         = Category::where( 'type', $type )->get();
        }
        //$countries_list                 = Country::get();

        $type_array[ 'graduation' ]     = GRADUATION_YEAR;
        $type_array[ 'intake' ]         = INTAKE;
        // $type_array[ 'languages' ]      = LANGUAGES;     
        // $type_array[ 'countries' ]      = $countries_list;
        
        return $type_array;
    }

    function update_student_profile( Request $request ) {
        
        $return_data[ 'status' ] 	    = false;
        $student_id                     = $request->input( 'student_id' );
        $fname 		                    = $request->input( 'fname' );
        $lname 		                    = $request->input( 'lname' );
        $name                           = $fname . ' ' . $lname;
        $mobile 					    = $request->input( 'mobile' );
        $intake                         = $request->input( 'intake' );
        $country_of_origin              = $request->input( 'country_of_origin' );
        $country_id                     = $request->input( 'country_id' );
        $qualification                  = $request->input( 'qualification' );
        $study_area                     = $request->input( 'study_area' );
        $graduation_year                = $request->input( 'graduation_year' );
        $language                       = collect( $request->input( 'language' ) )->toJson();
        //$language                       = json_encode( $language );

        $country_info                   = Country::find( $country_id );
        $country                        = $country_info->name;

        $student_data                   = [ 
                                            'name'                  => $name,
                                            'fname'                 => $fname,
                                            'lname'                 => $lname,
                                            'mobile'                => $mobile,
                                            'intake'                => $intake,
                                            'country_of_origin'     => $country_of_origin,
                                            'country_id'            => $country_id,
                                            'qualification'         => $qualification,
                                            'study_area'            => $study_area,
                                            'graduation_year'       => $graduation_year,
                                            'country'               => $country,
                                            'language'              => $language
                                            ];
        //INSERT STUDENT
        $updated                        = Student::where( 'id', $student_id )->update( $student_data );
        if( $updated ) {
            $return_data[ 'status' ] 	= true;
        }

        return $return_data;
    }

    function change_email( Request $request ) {
        
        $return_data[ 'status' ] 	    = false;
        $student_id                     = $request->input( 'student_id' );
        $email                          = $request->input( 'email' );

        $student_info                   = Student::where( [ 'id' => $student_id ] )->get()->first();
        if( !empty( $student_info ) ) {
            $prev_email                     = $student_info->email;
            $email_unique_key 		        = get_rand_alphanumeric(30);
            //UPDATE
            Student::where( 'id', $student_id )->update( [ 'email_unique_key'=> $email_unique_key, 'change_email'=> $email ] );
            //MAIL
            Mail::to( $email )->send( new ChangeEmailMail( $email_unique_key ) );
            $return_data[ 'status' ] 	    = true;
        }

        return $return_data;
    }

    function confirm_email( Request $request, $email_unique_key ) {
        
        $return_data[ 'status' ] 	    = false;
        $student_info                   = Student::where( [ 'email_unique_key' => $email_unique_key ] )->get()->first();
        if( !empty( $student_info ) && !empty( $email_unique_key ) ) {
            $student_id                 = $student_info->id;
            $email                      = $student_info->change_email;
            //UPDATE
            Student::where( 'id', $student_id )->update( [ 'email_unique_key'=> '', 'email'=> $email, 'change_email'=> '' ] );
            $return_data[ 'status' ]    = true;
        }

        return $return_data;
    }

    function send_feedback( Request $request ) {
        
        $name                           = $request->input( 'name' );
        $email                          = $request->input( 'email' );
        $message                        = $request->input( 'message' );

        $feedback_data[ 'name' ]        = $name;
        $feedback_data[ 'email' ]       = $email;
        $feedback_data[ 'message' ]     = $message;
        //MAIL
        Mail::to( SUPPORT_EMAIL )->send( new FeedbackMail( $feedback_data ) );
        $return_data[ 'status' ] 	    = true;

        return $return_data;
    }

}