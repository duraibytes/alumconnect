<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApprovalConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $student_info )
    {
        $this->student_info     = $student_info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject         = config('app.name').' / Approval Confirmation';
        
        return $this->subject( $subject )
        ->view( 'mail._approval_confirmation_mail', [ 'student_info' => $this->student_info ] );
    }
}
