<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChangeEmailMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $unique_key )
    {
        $this->unique_key     = $unique_key;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject         = config('app.name').' / Please Confirm Your E-mail Address';
        
        return $this->subject( $subject )
        ->view( 'mail._change_email_mail', [ 'unique_key' => $this->unique_key ] );
    }
}
