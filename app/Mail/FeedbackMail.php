<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $feedback_data )
    {
        $this->feedback_data     = $feedback_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject         = config('app.name').' / Feedback';
        
        return $this->subject( $subject )
        ->view( 'mail._feedback_mail', [ 'feedback_data' => $this->feedback_data ] );
    }
}
