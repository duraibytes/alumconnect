<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $password, $type, $email )
    {
        $this->password     = $password;
        $this->type         = $type;
        $this->email        = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        if( isset( $this->type ) && $this->type == 'otp' ) {

            $subject         = config('app.name').' / Otp received';
            return $this->subject( $subject )
            ->markdown( 'mail._otp_mail',[ 'otp' => $this->password ] );

        } else {
            if( isset( $this->type ) && $this->type == 'reset' ) {
                $subject         = config('app.name').' / Reset your password';
            } else {
                $subject         = config('app.name').' / New password received';
            }
            
            return $this->subject( $subject )
            ->markdown( 'mail._password_mail',[ 'password' => $this->password, 'email' => $this->email ] );
        }
       
    }
}
