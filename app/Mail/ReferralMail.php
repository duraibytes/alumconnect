<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReferralMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $referral_info )
    {
        $this->referral_info     = $referral_info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject         = config('app.name').' / Referral';
        
        return $this->subject( $subject )
        ->view( 'mail._referral_mail', [ 'referral_info' => $this->referral_info ] );
    }
}
