<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'type', 'page', 'name', 'updated_by', 'updated_at', 'parent_id'
    ];

    // public function user_category() {
    //     return $this->hasMany( 'App\Model\Category', 'code_id' )->whereNull( 'code_options.approved_at' );
        
    // }
    public function subcategory_list()
    {
        return $this->hasMany( Category::class, 'parent_id', 'id' );
    }
    public function category_info()
    {
        return $this->belongsTo( Category::class, 'parent_id', 'id' );
    }

    public function scopeTag( Builder $query ) {
        return $query->where( 'type', '=', 'TAG' );
    }

    public function tag_list()
    {
        return $this->hasMany( Category::class, 'parent_id', 'id' )->where( 'type', 'TAG' );
    }
}
