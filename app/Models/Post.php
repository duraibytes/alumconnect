<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{
    use HasFactory;
    protected $fillable = [ 'title', 'category_id', 'video_url', 'description', 'post_image', 'status', 'slug', 'created_by', 'type' ];
    protected $table    = 'post';

    public function category_info()
    {
        return $this->belongsTo( Category::class, 'category_id', 'id' );
    }
}
