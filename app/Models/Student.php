<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Sanctum\HasApiTokens;

class Student extends Model
{
    use HasApiTokens, HasFactory;
    protected $fillable = [
        'name', 'email', 'password', 'type', 'category', 'fname', 'lname', 'mobile', 'referred_by_user_id', 'referral_date', 'referral_status', 'referral_code', 'status', 'country_id', 'last_login', 'email_verified_at', 'student_id_file', 'student_id_s3'
    ];

    public function scopeJoinCategory( Builder $query ) {
        return $query->leftJoin( 'categories', function( $join ) {
            $join->on( 'students.category', '=', 'categories.id' );
        } );
    }

    public function category_info()
    {
        return $this->belongsTo( Category::class, 'category', 'id' );
    }

    public function country_info()
    {
        return $this->belongsTo( Country::class, 'country_id', 'id' );
    }

    public function referred_info()
    {
        return $this->belongsTo( Student::class, 'referred_by_user_id', 'id' );
    }

    public function scopeOnlyAdmin( Builder $query ) {
        return $query->where( 'students.type', '=', 'A' );
    }

    public function scopeOnlyStudent( Builder $query ) {
        return $query->where( 'students.type', '=', 'S' );
    }

    public function scopeJoinReferred( Builder $query ) {
        return $query->Join( 'students as ref', function( $join ) {
            $join->on( 'students.referred_by_user_id', '=', 'ref.id' );
        } );
        return $query->where( 'students.referred_by_user_id', '!=', 0 );
    }

    public function scopeJoinCountry( Builder $query ) {
        return $query->leftJoin( 'countries', function( $join ) {
            $join->on( 'students.country_id', '=', 'countries.id' );
        } );
    }

    public function scopeSearch( Builder $query, $search ) {

        if( empty( $search ) ) {
            return $query;
        }

        return  $query->where( function( $query ) use( $search ) {
                    $query->where( 'students.name', 'like', "%{$search}%" )
                        ->orWhere( 'students.email', 'like', "%{$search}%" )
                        ->orWhere( 'students.fname', 'like', "%{$search}%" )
                        ->orWhere( 'countries.name', 'like', "%{$search}%" );
                        
                } ); 
    }
}
