<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'category', 'fname', 'referred_by_user_id', 'status', 'genereted_email_tokens', 'profile_photo', 'country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeJoinCategory( Builder $query ) {
        return $query->leftJoin( 'categories', function( $join ) {
            $join->on( 'users.category', '=', 'categories.id' );
        } );
    }

    public function category_info()
    {
        return $this->belongsTo( Category::class, 'category', 'id' );
    }

    public function scopeOnlyAdmin( Builder $query ) {
        return $query->where( 'users.type', '=', 'A' );
    }

    public function scopeOnlyStudent( Builder $query ) {
        return $query->where( 'users.type', '=', 'S' );
    }

    public function scopeSearch( Builder $query, $search ) {

        if( empty( $search ) ) {
            return $query;
        }

        return  $query->where( function( $query ) use( $search ) {
                    $query->where( 'name', 'like', "%{$search}%" )
                        ->orWhere( 'email', 'like', "%{$search}%" )
                        ->orWhere( 'fname', 'like', "%{$search}%" );
                        
                } ); 
    }

}
