<?php return array (
  'app' => 
  array (
    'name' => 'ALUMCONNECT',
    'env' => 'local',
    'debug' => true,
    'url' => 'http://localhost',
    'asset_url' => NULL,
    'timezone' => 'UTC',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'faker_locale' => 'en_US',
    'key' => 'base64:E+rFIl3pFQJTOf/Bfz/0ERAu1bytmpfVexLz5WiQo+o=',
    'cipher' => 'AES-256-CBC',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'App\\Providers\\AppServiceProvider',
      23 => 'App\\Providers\\AuthServiceProvider',
      24 => 'App\\Providers\\EventServiceProvider',
      25 => 'App\\Providers\\RouteServiceProvider',
      26 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Arr' => 'Illuminate\\Support\\Arr',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Broadcast' => 'Illuminate\\Support\\Facades\\Broadcast',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Http' => 'Illuminate\\Support\\Facades\\Http',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'Str' => 'Illuminate\\Support\\Str',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
    ),
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'web',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'api' => 
      array (
        'driver' => 'token',
        'provider' => 'users',
        'hash' => false,
      ),
      'sanctum' => 
      array (
        'driver' => 'sanctum',
        'provider' => NULL,
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\Models\\User',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
        'throttle' => 60,
      ),
    ),
    'password_timeout' => 10800,
  ),
  'broadcasting' => 
  array (
    'default' => 'log',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '',
        'secret' => '',
        'app_id' => '',
        'options' => 
        array (
          'cluster' => 'mt1',
          'useTLS' => true,
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
        'serialize' => false,
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\framework/cache/data',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'cache',
      ),
      'dynamodb' => 
      array (
        'driver' => 'dynamodb',
        'key' => 'AKIAIZOWYP7Y4URK3Z6A',
        'secret' => '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',
        'region' => 'us-west-1',
        'table' => 'cache',
        'endpoint' => NULL,
      ),
    ),
    'prefix' => 'alumconnect_cache',
  ),
  'constants' => 
  array (
    'ADMIN_NAME' => 'administrator',
    'LANGUAGES' => 
    array (
      'ab' => 
      array (
        'name' => 'Abkhaz',
        'nativeName' => 'аҧсуа',
      ),
      'aa' => 
      array (
        'name' => 'Afar',
        'nativeName' => 'Afaraf',
      ),
      'af' => 
      array (
        'name' => 'Afrikaans',
        'nativeName' => 'Afrikaans',
      ),
      'ak' => 
      array (
        'name' => 'Akan',
        'nativeName' => 'Akan',
      ),
      'sq' => 
      array (
        'name' => 'Albanian',
        'nativeName' => 'Shqip',
      ),
      'am' => 
      array (
        'name' => 'Amharic',
        'nativeName' => 'አማርኛ',
      ),
      'ar' => 
      array (
        'name' => 'Arabic',
        'nativeName' => 'العربية',
      ),
      'an' => 
      array (
        'name' => 'Aragonese',
        'nativeName' => 'Aragonés',
      ),
      'hy' => 
      array (
        'name' => 'Armenian',
        'nativeName' => 'Հայերեն',
      ),
      'as' => 
      array (
        'name' => 'Assamese',
        'nativeName' => 'অসমীয়া',
      ),
      'av' => 
      array (
        'name' => 'Avaric',
        'nativeName' => 'авар мацӀ, магӀарул мацӀ',
      ),
      'ae' => 
      array (
        'name' => 'Avestan',
        'nativeName' => 'avesta',
      ),
      'ay' => 
      array (
        'name' => 'Aymara',
        'nativeName' => 'aymar aru',
      ),
      'az' => 
      array (
        'name' => 'Azerbaijani',
        'nativeName' => 'azərbaycan dili',
      ),
      'bm' => 
      array (
        'name' => 'Bambara',
        'nativeName' => 'bamanankan',
      ),
      'ba' => 
      array (
        'name' => 'Bashkir',
        'nativeName' => 'башҡорт теле',
      ),
      'eu' => 
      array (
        'name' => 'Basque',
        'nativeName' => 'euskara, euskera',
      ),
      'be' => 
      array (
        'name' => 'Belarusian',
        'nativeName' => 'Беларуская',
      ),
      'bn' => 
      array (
        'name' => 'Bengali',
        'nativeName' => 'বাংলা',
      ),
      'bh' => 
      array (
        'name' => 'Bihari',
        'nativeName' => 'भोजपुरी',
      ),
      'bi' => 
      array (
        'name' => 'Bislama',
        'nativeName' => 'Bislama',
      ),
      'bs' => 
      array (
        'name' => 'Bosnian',
        'nativeName' => 'bosanski jezik',
      ),
      'br' => 
      array (
        'name' => 'Breton',
        'nativeName' => 'brezhoneg',
      ),
      'bg' => 
      array (
        'name' => 'Bulgarian',
        'nativeName' => 'български език',
      ),
      'my' => 
      array (
        'name' => 'Burmese',
        'nativeName' => 'ဗမာစာ',
      ),
      'ca' => 
      array (
        'name' => 'Catalan; Valencian',
        'nativeName' => 'Català',
      ),
      'ch' => 
      array (
        'name' => 'Chamorro',
        'nativeName' => 'Chamoru',
      ),
      'ce' => 
      array (
        'name' => 'Chechen',
        'nativeName' => 'нохчийн мотт',
      ),
      'ny' => 
      array (
        'name' => 'Chichewa; Chewa; Nyanja',
        'nativeName' => 'chiCheŵa, chinyanja',
      ),
      'zh' => 
      array (
        'name' => 'Chinese',
        'nativeName' => '中文 (Zhōngwén), 汉语, 漢語',
      ),
      'cv' => 
      array (
        'name' => 'Chuvash',
        'nativeName' => 'чӑваш чӗлхи',
      ),
      'kw' => 
      array (
        'name' => 'Cornish',
        'nativeName' => 'Kernewek',
      ),
      'co' => 
      array (
        'name' => 'Corsican',
        'nativeName' => 'corsu, lingua corsa',
      ),
      'cr' => 
      array (
        'name' => 'Cree',
        'nativeName' => 'ᓀᐦᐃᔭᐍᐏᐣ',
      ),
      'hr' => 
      array (
        'name' => 'Croatian',
        'nativeName' => 'hrvatski',
      ),
      'cs' => 
      array (
        'name' => 'Czech',
        'nativeName' => 'česky, čeština',
      ),
      'da' => 
      array (
        'name' => 'Danish',
        'nativeName' => 'dansk',
      ),
      'dv' => 
      array (
        'name' => 'Divehi; Dhivehi; Maldivian;',
        'nativeName' => 'ދިވެހި',
      ),
      'nl' => 
      array (
        'name' => 'Dutch',
        'nativeName' => 'Nederlands, Vlaams',
      ),
      'en' => 
      array (
        'name' => 'English',
        'nativeName' => 'English',
      ),
      'eo' => 
      array (
        'name' => 'Esperanto',
        'nativeName' => 'Esperanto',
      ),
      'et' => 
      array (
        'name' => 'Estonian',
        'nativeName' => 'eesti, eesti keel',
      ),
      'ee' => 
      array (
        'name' => 'Ewe',
        'nativeName' => 'Eʋegbe',
      ),
      'fo' => 
      array (
        'name' => 'Faroese',
        'nativeName' => 'føroyskt',
      ),
      'fj' => 
      array (
        'name' => 'Fijian',
        'nativeName' => 'vosa Vakaviti',
      ),
      'fi' => 
      array (
        'name' => 'Finnish',
        'nativeName' => 'suomi, suomen kieli',
      ),
      'fr' => 
      array (
        'name' => 'French',
        'nativeName' => 'français, langue française',
      ),
      'ff' => 
      array (
        'name' => 'Fula; Fulah; Pulaar; Pular',
        'nativeName' => 'Fulfulde, Pulaar, Pular',
      ),
      'gl' => 
      array (
        'name' => 'Galician',
        'nativeName' => 'Galego',
      ),
      'ka' => 
      array (
        'name' => 'Georgian',
        'nativeName' => 'ქართული',
      ),
      'de' => 
      array (
        'name' => 'German',
        'nativeName' => 'Deutsch',
      ),
      'el' => 
      array (
        'name' => 'Greek, Modern',
        'nativeName' => 'Ελληνικά',
      ),
      'gn' => 
      array (
        'name' => 'Guaraní',
        'nativeName' => 'Avañeẽ',
      ),
      'gu' => 
      array (
        'name' => 'Gujarati',
        'nativeName' => 'ગુજરાતી',
      ),
      'ht' => 
      array (
        'name' => 'Haitian; Haitian Creole',
        'nativeName' => 'Kreyòl ayisyen',
      ),
      'ha' => 
      array (
        'name' => 'Hausa',
        'nativeName' => 'Hausa, هَوُسَ',
      ),
      'he' => 
      array (
        'name' => 'Hebrew',
        'nativeName' => 'עברית',
      ),
      'iw' => 
      array (
        'name' => 'Hebrew',
        'nativeName' => 'עברית',
      ),
      'hz' => 
      array (
        'name' => 'Herero',
        'nativeName' => 'Otjiherero',
      ),
      'hi' => 
      array (
        'name' => 'Hindi',
        'nativeName' => 'हिन्दी, हिंदी',
      ),
      'ho' => 
      array (
        'name' => 'Hiri Motu',
        'nativeName' => 'Hiri Motu',
      ),
      'hu' => 
      array (
        'name' => 'Hungarian',
        'nativeName' => 'Magyar',
      ),
      'ia' => 
      array (
        'name' => 'Interlingua',
        'nativeName' => 'Interlingua',
      ),
      'id' => 
      array (
        'name' => 'Indonesian',
        'nativeName' => 'Bahasa Indonesia',
      ),
      'ie' => 
      array (
        'name' => 'Interlingue',
        'nativeName' => 'Originally called Occidental; then Interlingue after WWII',
      ),
      'ga' => 
      array (
        'name' => 'Irish',
        'nativeName' => 'Gaeilge',
      ),
      'ig' => 
      array (
        'name' => 'Igbo',
        'nativeName' => 'Asụsụ Igbo',
      ),
      'ik' => 
      array (
        'name' => 'Inupiaq',
        'nativeName' => 'Iñupiaq, Iñupiatun',
      ),
      'io' => 
      array (
        'name' => 'Ido',
        'nativeName' => 'Ido',
      ),
      'is' => 
      array (
        'name' => 'Icelandic',
        'nativeName' => 'Íslenska',
      ),
      'it' => 
      array (
        'name' => 'Italian',
        'nativeName' => 'Italiano',
      ),
      'iu' => 
      array (
        'name' => 'Inuktitut',
        'nativeName' => 'ᐃᓄᒃᑎᑐᑦ',
      ),
      'ja' => 
      array (
        'name' => 'Japanese',
        'nativeName' => '日本語 (にほんご／にっぽんご)',
      ),
      'jv' => 
      array (
        'name' => 'Javanese',
        'nativeName' => 'basa Jawa',
      ),
      'kl' => 
      array (
        'name' => 'Kalaallisut, Greenlandic',
        'nativeName' => 'kalaallisut, kalaallit oqaasii',
      ),
      'kn' => 
      array (
        'name' => 'Kannada',
        'nativeName' => 'ಕನ್ನಡ',
      ),
      'kr' => 
      array (
        'name' => 'Kanuri',
        'nativeName' => 'Kanuri',
      ),
      'ks' => 
      array (
        'name' => 'Kashmiri',
        'nativeName' => 'कश्मीरी, كشميري‎',
      ),
      'kk' => 
      array (
        'name' => 'Kazakh',
        'nativeName' => 'Қазақ тілі',
      ),
      'km' => 
      array (
        'name' => 'Khmer',
        'nativeName' => 'ភាសាខ្មែរ',
      ),
      'ki' => 
      array (
        'name' => 'Kikuyu, Gikuyu',
        'nativeName' => 'Gĩkũyũ',
      ),
      'rw' => 
      array (
        'name' => 'Kinyarwanda',
        'nativeName' => 'Ikinyarwanda',
      ),
      'ky' => 
      array (
        'name' => 'Kirghiz, Kyrgyz',
        'nativeName' => 'кыргыз тили',
      ),
      'kv' => 
      array (
        'name' => 'Komi',
        'nativeName' => 'коми кыв',
      ),
      'kg' => 
      array (
        'name' => 'Kongo',
        'nativeName' => 'KiKongo',
      ),
      'ko' => 
      array (
        'name' => 'Korean',
        'nativeName' => '한국어 (韓國語), 조선말 (朝鮮語)',
      ),
      'ku' => 
      array (
        'name' => 'Kurdish',
        'nativeName' => 'Kurdî, كوردی‎',
      ),
      'kj' => 
      array (
        'name' => 'Kwanyama, Kuanyama',
        'nativeName' => 'Kuanyama',
      ),
      'la' => 
      array (
        'name' => 'Latin',
        'nativeName' => 'latine, lingua latina',
      ),
      'lb' => 
      array (
        'name' => 'Luxembourgish, Letzeburgesch',
        'nativeName' => 'Lëtzebuergesch',
      ),
      'lg' => 
      array (
        'name' => 'Luganda',
        'nativeName' => 'Luganda',
      ),
      'li' => 
      array (
        'name' => 'Limburgish, Limburgan, Limburger',
        'nativeName' => 'Limburgs',
      ),
      'ln' => 
      array (
        'name' => 'Lingala',
        'nativeName' => 'Lingála',
      ),
      'lo' => 
      array (
        'name' => 'Lao',
        'nativeName' => 'ພາສາລາວ',
      ),
      'lt' => 
      array (
        'name' => 'Lithuanian',
        'nativeName' => 'lietuvių kalba',
      ),
      'lu' => 
      array (
        'name' => 'Luba-Katanga',
        'nativeName' => '',
      ),
      'lv' => 
      array (
        'name' => 'Latvian',
        'nativeName' => 'latviešu valoda',
      ),
      'gv' => 
      array (
        'name' => 'Manx',
        'nativeName' => 'Gaelg, Gailck',
      ),
      'mk' => 
      array (
        'name' => 'Macedonian',
        'nativeName' => 'македонски јазик',
      ),
      'mg' => 
      array (
        'name' => 'Malagasy',
        'nativeName' => 'Malagasy fiteny',
      ),
      'ms' => 
      array (
        'name' => 'Malay',
        'nativeName' => 'bahasa Melayu, بهاس ملايو‎',
      ),
      'ml' => 
      array (
        'name' => 'Malayalam',
        'nativeName' => 'മലയാളം',
      ),
      'mt' => 
      array (
        'name' => 'Maltese',
        'nativeName' => 'Malti',
      ),
      'mi' => 
      array (
        'name' => 'Māori',
        'nativeName' => 'te reo Māori',
      ),
      'mr' => 
      array (
        'name' => 'Marathi (Marāṭhī)',
        'nativeName' => 'मराठी',
      ),
      'mh' => 
      array (
        'name' => 'Marshallese',
        'nativeName' => 'Kajin M̧ajeļ',
      ),
      'mn' => 
      array (
        'name' => 'Mongolian',
        'nativeName' => 'монгол',
      ),
      'na' => 
      array (
        'name' => 'Nauru',
        'nativeName' => 'Ekakairũ Naoero',
      ),
      'nv' => 
      array (
        'name' => 'Navajo, Navaho',
        'nativeName' => 'Diné bizaad, Dinékʼehǰí',
      ),
      'nb' => 
      array (
        'name' => 'Norwegian Bokmål',
        'nativeName' => 'Norsk bokmål',
      ),
      'nd' => 
      array (
        'name' => 'North Ndebele',
        'nativeName' => 'isiNdebele',
      ),
      'ne' => 
      array (
        'name' => 'Nepali',
        'nativeName' => 'नेपाली',
      ),
      'ng' => 
      array (
        'name' => 'Ndonga',
        'nativeName' => 'Owambo',
      ),
      'nn' => 
      array (
        'name' => 'Norwegian Nynorsk',
        'nativeName' => 'Norsk nynorsk',
      ),
      'no' => 
      array (
        'name' => 'Norwegian',
        'nativeName' => 'Norsk',
      ),
      'ii' => 
      array (
        'name' => 'Nuosu',
        'nativeName' => 'ꆈꌠ꒿ Nuosuhxop',
      ),
      'nr' => 
      array (
        'name' => 'South Ndebele',
        'nativeName' => 'isiNdebele',
      ),
      'oc' => 
      array (
        'name' => 'Occitan',
        'nativeName' => 'Occitan',
      ),
      'oj' => 
      array (
        'name' => 'Ojibwe, Ojibwa',
        'nativeName' => 'ᐊᓂᔑᓈᐯᒧᐎᓐ',
      ),
      'cu' => 
      array (
        'name' => 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic',
        'nativeName' => 'ѩзыкъ словѣньскъ',
      ),
      'om' => 
      array (
        'name' => 'Oromo',
        'nativeName' => 'Afaan Oromoo',
      ),
      'or' => 
      array (
        'name' => 'Oriya',
        'nativeName' => 'ଓଡ଼ିଆ',
      ),
      'os' => 
      array (
        'name' => 'Ossetian, Ossetic',
        'nativeName' => 'ирон æвзаг',
      ),
      'pa' => 
      array (
        'name' => 'Panjabi, Punjabi',
        'nativeName' => 'ਪੰਜਾਬੀ, پنجابی‎',
      ),
      'pi' => 
      array (
        'name' => 'Pāli',
        'nativeName' => 'पाऴि',
      ),
      'fa' => 
      array (
        'name' => 'Persian',
        'nativeName' => 'فارسی',
      ),
      'pl' => 
      array (
        'name' => 'Polish',
        'nativeName' => 'polski',
      ),
      'ps' => 
      array (
        'name' => 'Pashto, Pushto',
        'nativeName' => 'پښتو',
      ),
      'pt' => 
      array (
        'name' => 'Portuguese',
        'nativeName' => 'Português',
      ),
      'qu' => 
      array (
        'name' => 'Quechua',
        'nativeName' => 'Runa Simi, Kichwa',
      ),
      'rm' => 
      array (
        'name' => 'Romansh',
        'nativeName' => 'rumantsch grischun',
      ),
      'rn' => 
      array (
        'name' => 'Kirundi',
        'nativeName' => 'kiRundi',
      ),
      'ro' => 
      array (
        'name' => 'Romanian, Moldavian, Moldovan',
        'nativeName' => 'română',
      ),
      'ru' => 
      array (
        'name' => 'Russian',
        'nativeName' => 'русский язык',
      ),
      'sa' => 
      array (
        'name' => 'Sanskrit (Saṁskṛta)',
        'nativeName' => 'संस्कृतम्',
      ),
      'sc' => 
      array (
        'name' => 'Sardinian',
        'nativeName' => 'sardu',
      ),
      'sd' => 
      array (
        'name' => 'Sindhi',
        'nativeName' => 'सिन्धी, سنڌي، سندھی‎',
      ),
      'se' => 
      array (
        'name' => 'Northern Sami',
        'nativeName' => 'Davvisámegiella',
      ),
      'sm' => 
      array (
        'name' => 'Samoan',
        'nativeName' => 'gagana faa Samoa',
      ),
      'sg' => 
      array (
        'name' => 'Sango',
        'nativeName' => 'yângâ tî sängö',
      ),
      'sr' => 
      array (
        'name' => 'Serbian',
        'nativeName' => 'српски језик',
      ),
      'gd' => 
      array (
        'name' => 'Scottish Gaelic; Gaelic',
        'nativeName' => 'Gàidhlig',
      ),
      'sn' => 
      array (
        'name' => 'Shona',
        'nativeName' => 'chiShona',
      ),
      'si' => 
      array (
        'name' => 'Sinhala, Sinhalese',
        'nativeName' => 'සිංහල',
      ),
      'sk' => 
      array (
        'name' => 'Slovak',
        'nativeName' => 'slovenčina',
      ),
      'sl' => 
      array (
        'name' => 'Slovene',
        'nativeName' => 'slovenščina',
      ),
      'so' => 
      array (
        'name' => 'Somali',
        'nativeName' => 'Soomaaliga, af Soomaali',
      ),
      'st' => 
      array (
        'name' => 'Southern Sotho',
        'nativeName' => 'Sesotho',
      ),
      'es' => 
      array (
        'name' => 'Spanish; Castilian',
        'nativeName' => 'español, castellano',
      ),
      'su' => 
      array (
        'name' => 'Sundanese',
        'nativeName' => 'Basa Sunda',
      ),
      'sw' => 
      array (
        'name' => 'Swahili',
        'nativeName' => 'Kiswahili',
      ),
      'ss' => 
      array (
        'name' => 'Swati',
        'nativeName' => 'SiSwati',
      ),
      'sv' => 
      array (
        'name' => 'Swedish',
        'nativeName' => 'svenska',
      ),
      'ta' => 
      array (
        'name' => 'Tamil',
        'nativeName' => 'தமிழ்',
      ),
      'te' => 
      array (
        'name' => 'Telugu',
        'nativeName' => 'తెలుగు',
      ),
      'tg' => 
      array (
        'name' => 'Tajik',
        'nativeName' => 'тоҷикӣ, toğikī, تاجیکی‎',
      ),
      'th' => 
      array (
        'name' => 'Thai',
        'nativeName' => 'ไทย',
      ),
      'ti' => 
      array (
        'name' => 'Tigrinya',
        'nativeName' => 'ትግርኛ',
      ),
      'bo' => 
      array (
        'name' => 'Tibetan Standard, Tibetan, Central',
        'nativeName' => 'བོད་ཡིག',
      ),
      'tk' => 
      array (
        'name' => 'Turkmen',
        'nativeName' => 'Türkmen, Түркмен',
      ),
      'tl' => 
      array (
        'name' => 'Tagalog',
        'nativeName' => 'Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔',
      ),
      'tn' => 
      array (
        'name' => 'Tswana',
        'nativeName' => 'Setswana',
      ),
      'to' => 
      array (
        'name' => 'Tonga (Tonga Islands)',
        'nativeName' => 'faka Tonga',
      ),
      'tr' => 
      array (
        'name' => 'Turkish',
        'nativeName' => 'Türkçe',
      ),
      'ts' => 
      array (
        'name' => 'Tsonga',
        'nativeName' => 'Xitsonga',
      ),
      'tt' => 
      array (
        'name' => 'Tatar',
        'nativeName' => 'татарча, tatarça, تاتارچا‎',
      ),
      'tw' => 
      array (
        'name' => 'Twi',
        'nativeName' => 'Twi',
      ),
      'ty' => 
      array (
        'name' => 'Tahitian',
        'nativeName' => 'Reo Tahiti',
      ),
      'ug' => 
      array (
        'name' => 'Uighur, Uyghur',
        'nativeName' => 'Uyƣurqə, ئۇيغۇرچە‎',
      ),
      'uk' => 
      array (
        'name' => 'Ukrainian',
        'nativeName' => 'українська',
      ),
      'ur' => 
      array (
        'name' => 'Urdu',
        'nativeName' => 'اردو',
      ),
      'uz' => 
      array (
        'name' => 'Uzbek',
        'nativeName' => 'zbek, Ўзбек, أۇزبېك‎',
      ),
      've' => 
      array (
        'name' => 'Venda',
        'nativeName' => 'Tshivenḓa',
      ),
      'vi' => 
      array (
        'name' => 'Vietnamese',
        'nativeName' => 'Tiếng Việt',
      ),
      'vo' => 
      array (
        'name' => 'Volapük',
        'nativeName' => 'Volapük',
      ),
      'wa' => 
      array (
        'name' => 'Walloon',
        'nativeName' => 'Walon',
      ),
      'cy' => 
      array (
        'name' => 'Welsh',
        'nativeName' => 'Cymraeg',
      ),
      'wo' => 
      array (
        'name' => 'Wolof',
        'nativeName' => 'Wollof',
      ),
      'fy' => 
      array (
        'name' => 'Western Frisian',
        'nativeName' => 'Frysk',
      ),
      'xh' => 
      array (
        'name' => 'Xhosa',
        'nativeName' => 'isiXhosa',
      ),
      'yi' => 
      array (
        'name' => 'Yiddish',
        'nativeName' => 'ייִדיש',
      ),
      'yo' => 
      array (
        'name' => 'Yoruba',
        'nativeName' => 'Yorùbá',
      ),
      'za' => 
      array (
        'name' => 'Zhuang, Chuang',
        'nativeName' => 'Saɯ cueŋƅ, Saw cuengh',
      ),
    ),
    'INTAKE' => 
    array (
      0 => '2021 T1',
      1 => '2021 T2',
      2 => '2021 T3',
      3 => '2022 T1',
      4 => '2022 T2',
      5 => '2022 T3',
      6 => '2023 T1',
      7 => '2023 T2',
      8 => '2023 T3',
    ),
    'GRADUATION_YEAR' => 
    array (
      2021 => '2021',
      2022 => 2022,
      2023 => 2023,
      2024 => 2024,
    ),
    'SUPPORT_EMAIL' => 'duraibytes@gmail.com',
  ),
  'cors' => 
  array (
    'paths' => 
    array (
      0 => 'api/*',
    ),
    'allowed_methods' => 
    array (
      0 => '*',
    ),
    'allowed_origins' => 
    array (
      0 => '*',
    ),
    'allowed_origins_patterns' => 
    array (
    ),
    'allowed_headers' => 
    array (
      0 => '*',
    ),
    'exposed_headers' => 
    array (
    ),
    'max_age' => 0,
    'supports_credentials' => false,
  ),
  'database' => 
  array (
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'url' => NULL,
        'database' => 'alumconnect',
        'prefix' => '',
        'foreign_key_constraints' => true,
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'alumconnect',
        'username' => 'root',
        'password' => '',
        'unix_socket' => '',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => false,
        'engine' => NULL,
        'options' => 
        array (
        ),
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'alumconnect',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
        'prefix_indexes' => true,
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'alumconnect',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
        'prefix_indexes' => true,
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'client' => 'phpredis',
      'options' => 
      array (
        'cluster' => 'redis',
        'prefix' => 'alumconnect_database_',
      ),
      'default' => 
      array (
        'url' => NULL,
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => '0',
      ),
      'cache' => 
      array (
        'url' => NULL,
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => '1',
      ),
    ),
  ),
  'excel' => 
  array (
    'exports' => 
    array (
      'chunk_size' => 1000,
      'pre_calculate_formulas' => false,
      'strict_null_comparison' => false,
      'csv' => 
      array (
        'delimiter' => ',',
        'enclosure' => '"',
        'line_ending' => '
',
        'use_bom' => false,
        'include_separator_line' => false,
        'excel_compatibility' => false,
      ),
      'properties' => 
      array (
        'creator' => '',
        'lastModifiedBy' => '',
        'title' => '',
        'description' => '',
        'subject' => '',
        'keywords' => '',
        'category' => '',
        'manager' => '',
        'company' => '',
      ),
    ),
    'imports' => 
    array (
      'read_only' => true,
      'ignore_empty' => false,
      'heading_row' => 
      array (
        'formatter' => 'slug',
      ),
      'csv' => 
      array (
        'delimiter' => ',',
        'enclosure' => '"',
        'escape_character' => '\\',
        'contiguous' => false,
        'input_encoding' => 'UTF-8',
      ),
      'properties' => 
      array (
        'creator' => '',
        'lastModifiedBy' => '',
        'title' => '',
        'description' => '',
        'subject' => '',
        'keywords' => '',
        'category' => '',
        'manager' => '',
        'company' => '',
      ),
    ),
    'extension_detector' => 
    array (
      'xlsx' => 'Xlsx',
      'xlsm' => 'Xlsx',
      'xltx' => 'Xlsx',
      'xltm' => 'Xlsx',
      'xls' => 'Xls',
      'xlt' => 'Xls',
      'ods' => 'Ods',
      'ots' => 'Ods',
      'slk' => 'Slk',
      'xml' => 'Xml',
      'gnumeric' => 'Gnumeric',
      'htm' => 'Html',
      'html' => 'Html',
      'csv' => 'Csv',
      'tsv' => 'Csv',
      'pdf' => 'Dompdf',
    ),
    'value_binder' => 
    array (
      'default' => 'Maatwebsite\\Excel\\DefaultValueBinder',
    ),
    'cache' => 
    array (
      'driver' => 'memory',
      'batch' => 
      array (
        'memory_limit' => 60000,
      ),
      'illuminate' => 
      array (
        'store' => NULL,
      ),
    ),
    'transactions' => 
    array (
      'handler' => 'db',
    ),
    'temporary_files' => 
    array (
      'local_path' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\framework/laravel-excel',
      'remote_disk' => NULL,
      'remote_prefix' => NULL,
      'force_resync_remote' => NULL,
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\app/public',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\app/public',
        'url' => 'http://localhost/storage',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => 'AKIAIZOWYP7Y4URK3Z6A',
        'secret' => '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',
        'region' => 'us-west-1',
        'bucket' => '2019jk02',
        'url' => NULL,
        'endpoint' => NULL,
      ),
    ),
    'links' => 
    array (
      'C:\\xampp\\htdocs\\lara\\alumconnect\\public\\storage' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\app/public',
    ),
  ),
  'flare' => 
  array (
    'key' => NULL,
    'reporting' => 
    array (
      'anonymize_ips' => true,
      'collect_git_information' => false,
      'report_queries' => true,
      'maximum_number_of_collected_queries' => 200,
      'report_query_bindings' => true,
      'report_view_data' => true,
      'grouping_type' => NULL,
    ),
    'send_logs_as_events' => true,
  ),
  'global' => 
  array (
    'LOGO_URL_ICON' => 'https://thecodingoven.com/0_PROJECTNS/ALUMCONNECT/public/images/logo/logo-letter-9.png',
    'LOGO_URL' => 'https://thecodingoven.com/0_PROJECTNS/ALUMCONNECT/public/images/logo/logo-letter-9.png',
    'LOGIN_PAGE_BG' => 'https://thecodingoven.com/0_PROJECTNS/ALUMCONNECT/public/images/bg/login-background.jpg',
  ),
  'hashing' => 
  array (
    'driver' => 'bcrypt',
    'bcrypt' => 
    array (
      'rounds' => 10,
    ),
    'argon' => 
    array (
      'memory' => 1024,
      'threads' => 2,
      'time' => 2,
    ),
  ),
  'ignition' => 
  array (
    'editor' => 'phpstorm',
    'theme' => 'light',
    'enable_share_button' => true,
    'register_commands' => false,
    'ignored_solution_providers' => 
    array (
      0 => 'Facade\\Ignition\\SolutionProviders\\MissingPackageSolutionProvider',
    ),
    'enable_runnable_solutions' => NULL,
    'remote_sites_path' => '',
    'local_sites_path' => '',
    'housekeeping_endpoint_prefix' => '_ignition',
  ),
  'logging' => 
  array (
    'default' => 'stack',
    'channels' => 
    array (
      'stack' => 
      array (
        'driver' => 'stack',
        'channels' => 
        array (
          0 => 'single',
        ),
        'ignore_exceptions' => false,
      ),
      'single' => 
      array (
        'driver' => 'single',
        'path' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\logs/laravel.log',
        'level' => 'debug',
      ),
      'daily' => 
      array (
        'driver' => 'daily',
        'path' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\logs/laravel.log',
        'level' => 'debug',
        'days' => 14,
      ),
      'slack' => 
      array (
        'driver' => 'slack',
        'url' => NULL,
        'username' => 'Laravel Log',
        'emoji' => ':boom:',
        'level' => 'critical',
      ),
      'papertrail' => 
      array (
        'driver' => 'monolog',
        'level' => 'debug',
        'handler' => 'Monolog\\Handler\\SyslogUdpHandler',
        'handler_with' => 
        array (
          'host' => NULL,
          'port' => NULL,
        ),
      ),
      'stderr' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\StreamHandler',
        'formatter' => NULL,
        'with' => 
        array (
          'stream' => 'php://stderr',
        ),
      ),
      'syslog' => 
      array (
        'driver' => 'syslog',
        'level' => 'debug',
      ),
      'errorlog' => 
      array (
        'driver' => 'errorlog',
        'level' => 'debug',
      ),
      'null' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\NullHandler',
      ),
      'emergency' => 
      array (
        'path' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\logs/laravel.log',
      ),
    ),
  ),
  'mail' => 
  array (
    'default' => 'smtp',
    'mailers' => 
    array (
      'smtp' => 
      array (
        'transport' => 'smtp',
        'host' => 'smtp.sendgrid.net',
        'port' => '587',
        'encryption' => 'tls',
        'username' => 'apikey',
        'password' => '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',
        'timeout' => NULL,
        'auth_mode' => NULL,
      ),
      'ses' => 
      array (
        'transport' => 'ses',
      ),
      'mailgun' => 
      array (
        'transport' => 'mailgun',
      ),
      'postmark' => 
      array (
        'transport' => 'postmark',
      ),
      'sendmail' => 
      array (
        'transport' => 'sendmail',
        'path' => '/usr/sbin/sendmail -bs',
      ),
      'log' => 
      array (
        'transport' => 'log',
        'channel' => NULL,
      ),
      'array' => 
      array (
        'transport' => 'array',
      ),
    ),
    'from' => 
    array (
      'address' => 'sivabala.dev@gmail.com',
      'name' => 'ALUMCONNECT',
    ),
    'markdown' => 
    array (
      'theme' => 'default',
      'paths' => 
      array (
        0 => 'C:\\xampp\\htdocs\\lara\\alumconnect\\resources\\views/vendor/mail',
      ),
    ),
  ),
  'permission' => 
  array (
    'models' => 
    array (
      'permission' => 'Spatie\\Permission\\Models\\Permission',
      'role' => 'Spatie\\Permission\\Models\\Role',
    ),
    'table_names' => 
    array (
      'roles' => 'roles',
      'permissions' => 'permissions',
      'model_has_permissions' => 'model_has_permissions',
      'model_has_roles' => 'model_has_roles',
      'role_has_permissions' => 'role_has_permissions',
    ),
    'column_names' => 
    array (
      'model_morph_key' => 'model_id',
    ),
    'display_permission_in_exception' => false,
    'display_role_in_exception' => false,
    'enable_wildcard_permission' => false,
    'cache' => 
    array (
      'expiration_time' => 
      DateInterval::__set_state(array(
         'y' => 0,
         'm' => 0,
         'd' => 0,
         'h' => 24,
         'i' => 0,
         's' => 0,
         'f' => 0.0,
         'weekday' => 0,
         'weekday_behavior' => 0,
         'first_last_day_of' => 0,
         'invert' => 0,
         'days' => false,
         'special_type' => 0,
         'special_amount' => 0,
         'have_weekday_relative' => 0,
         'have_special_relative' => 0,
      )),
      'key' => 'spatie.permission.cache',
      'model_key' => 'name',
      'store' => 'default',
    ),
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => 0,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'AKIAIZOWYP7Y4URK3Z6A',
        'secret' => 'C6xBvnWvbQy7zH+e5sIjpninqOmpMA2Rw0mDYNrD',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'suffix' => NULL,
        'region' => 'us-west-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => NULL,
      ),
    ),
    'failed' => 
    array (
      'driver' => 'database-uuids',
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'sanctum' => 
  array (
    'stateful' => 
    array (
      0 => 'localhost',
      1 => 'localhost:3000',
      2 => '127.0.0.1',
      3 => '127.0.0.1:8000',
      4 => '::1',
    ),
    'expiration' => NULL,
    'middleware' => 
    array (
      'verify_csrf_token' => 'App\\Http\\Middleware\\VerifyCsrfToken',
      'encrypt_cookies' => 'App\\Http\\Middleware\\EncryptCookies',
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
      'endpoint' => 'api.mailgun.net',
    ),
    'postmark' => 
    array (
      'token' => NULL,
    ),
    'ses' => 
    array (
      'key' => 'AKIAIZOWYP7Y4URK3Z6A',
      'secret' => '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',
      'region' => 'us-west-1',
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => '120',
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'alumconnect_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => NULL,
    'http_only' => true,
    'same_site' => 'lax',
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'alias' => 
    array (
    ),
    'dont_alias' => 
    array (
      0 => 'App\\Nova',
    ),
  ),
  'trustedproxy' => 
  array (
    'proxies' => NULL,
    'headers' => 94,
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => 'C:\\xampp\\htdocs\\lara\\alumconnect\\resources\\views',
      1 => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\/app/mail',
    ),
    'compiled' => 'C:\\xampp\\htdocs\\lara\\alumconnect\\storage\\framework\\views',
  ),
);
