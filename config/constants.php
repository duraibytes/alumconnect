<?php
if ( !defined( 'SUPPORT_EMAIL' ) ) { 
    define( 'SUPPORT_EMAIL', 'edwin.noel@gmail.com' );
}

if ( !defined( 'LANGUAGES' ) ) { 
    $jsonString                     = file_get_contents('https://thecodingoven.com/0_PROJECTNS/ALUMCONNECT/resources/lang/languages.json');
    $languages_list                 = json_decode( $jsonString, true );
    define( 'LANGUAGES', $languages_list );
}

if ( !defined( 'GRADUATION_YEAR' ) ) { 
    $graduation_start_year          = date( 'Y' );
    $graduation_end_year            = date( 'Y' ) + 3;
    $q_a    = array();
    $j      = 0;
    for( $i = $graduation_start_year; $i<= $graduation_end_year; $i++ ) {
        $q_a[$j]['label'] = $i;
        $q_a[$j]['value'] = $i;
        $j++;
        // $graduation_list[ $i ]['label']      = $i;
        // $graduation_list[ $i ]['value']      = $i;
    }
    define( 'GRADUATION_YEAR', $q_a );
}

if ( !defined( 'INTAKE' ) ) { 
    $intake_start_year              = date( 'Y' );
    $intake_end_year                = date( 'Y' ) + 2;
    $intake_list                    = array();
    $j = 0;
    for( $y = $intake_start_year; $y<= $intake_end_year; $y++ ) {
        for( $k = 1; $k<= 3; $k++ ) {
            //$intake_list[ $y . ' T' . $k ]      = $y . ' T' . $k;
            $inake_date = $y . ' T' . $k;
            $intake_list[$j]['label'] = $inake_date;
            $intake_list[$j]['value'] = $inake_date;
            $j++;
            // array_push($intake_list, $inake_date);
        }
    }
    define( 'INTAKE', $intake_list );
}

return [
    'LANGUAGES'     => $languages_list,
    'INTAKE'        => $intake_list,
    'SUPPORT_EMAIL' => 'edwin.noel@gmail.com',
];
/* return [
    'REFERRAL_STATUS' => [ 
                            'APPLICATION_STARTED' => 'APPLICATION_STARTED',
                            'APPLICATION_STARTED' => 'APPLICATION_STARTED',
                            'APPLICATION_STARTED' => 'APPLICATION_STARTED',
                            'APPLICATION_STARTED' => 'APPLICATION_STARTED',
                            'APPLICATION_STARTED' => 'APPLICATION_STARTED',
                            ]
]; */