<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCategoryToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string( 'type' )->nullable()->after( 'id' )->collation( 'utf8mb4_unicode_ci' )->comment( 'A= Admin, S= Staff' );
            $table->unsignedBigInteger( 'category' )->nullable()->after( 'type' )->collation( 'utf8mb4_unicode_ci' )->comment( 'from categories table' );
            $table->unsignedBigInteger( 'referred_by_user_id' )->after( 'remember_token' )->default( 0 );
            $table->string( 'status' )->default( 0 )->comment( '0= NEW, 1= ACTIVE, 2=INACTIVE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn( 'type' );
            $table->dropColumn( 'category' );
            $table->dropColumn( 'referred_by_user_id' );
            $table->dropColumn( 'status' );
        });
    }
}
