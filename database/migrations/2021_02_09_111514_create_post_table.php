<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->id();
            $table->string( 'title' );
            $table->string( 'type' )->default( 'BLOG' );
            $table->unsignedBigInteger( 'category_id' );

            $table->foreign( 'category_id' )->references('id')->on('categories');
            $table->longText( 'video_url' )->nullable();
            $table->longText( 'description' )->nullable();
            $table->longText( 'post_image' )->nullable();
            $table->string( 'status' )->default( 'DRAFT' )->comment( 'DRAFT, PUBLISHED, ARCHIVED');
            $table->string( 'slug' )->nullable();
            
            $table->unsignedBigInteger( 'created_by' )->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
