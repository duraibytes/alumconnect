<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCountryIdToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string( 'fname' )->after( 'name' )->nullable();
            $table->dateTime( 'last_login' )->nullable();
            $table->unsignedBigInteger( 'country_id' )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('fname');
            $table->dropColumn('last_login');
            $table->dropColumn('country_id');
        });
    }
}
