<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnfieldsToStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string( 'lname' )->after( 'fname' )->nullable();
            $table->date( 'dob' )->after( 'lname' )->nullable();
            $table->string( 'country' )->after( 'dob' )->nullable();
            $table->date( 'referral_date' )->after( 'referred_by_user_id' )->nullable();
            $table->string( 'referral_status' )->after( 'referral_date' )->comment( 'REFERRED > APPLIED' );
            $table->integer( 'referral_payment_status' )->after( 'referral_status' )->default( 0 )->comment( '0 = NOT PAID, 1 = PAID' );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('lname');
            $table->dropColumn('dob');
            $table->dropColumn('country');
            $table->dropColumn('referral_date');
            $table->dropColumn('referral_status');
            $table->dropColumn('referral_payment_status');
        });
    }
}
