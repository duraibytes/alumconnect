@extends( 'layouts.login_layout' )

@section( 'content' )
<!--begin::Login Sign in form-->
<div class="login-signin">
    <div class="mb-20">
        <h3>Sign In To Admin</h3>
        <p class="opacity-60 font-weight-bold">Enter your details to login to your account:</p>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session( 'status' ) }}
            </div>
        @endif
    </div>
    
    <form method="POST" action="{{ route('login') }}" class="form" >
        @csrf
        <div class="form-group">
            {{-- <input class="form-control h-auto text-white placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8 mb-5" 
            type="text" placeholder="Email" name="username" autocomplete="off" /> --}}
            <input id="email" type="email" class="form-control h-auto text-white placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8 mb-5  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email">
        </div>
            @error('email')
                <div class="alert alert-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
            @enderror
        <div class="form-group">
            <input class="form-control h-auto text-white placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8 mb-5 @error('password') is-invalid @enderror" type="password" placeholder="Password" name="password" />
        </div>
        @error('password')
            <div class="alert alert-danger" role="alert">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
        <div class="form-group d-flex flex-wrap justify-content-between align-items-center px-8">
            <div class="checkbox-inline">
                <label class="checkbox checkbox-outline checkbox-white text-white m-0">
                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                <span></span>Remember me</label>
            </div>
            @if (Route::has('password.request'))
                <a class="btn btn-link text-white font-weight-bold" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
        </div>
        <div class="form-group text-center mt-10">
            <button id="kt_login_signin_submit" class="btn btn-pill btn-outline-white font-weight-bold opacity-90 px-15 py-3" type="submit">
                Sign In
            </button>
        </div>
    </form>
    {{-- <div class="mt-10">
        <span class="opacity-70 mr-4">Don't have an account yet?</span>
        <a href="javascript:;" id="kt_login_signup" class="text-white font-weight-bold">Sign Up</a>
    </div> --}}
</div>
<!--end::Login Sign in form-->


@endsection
