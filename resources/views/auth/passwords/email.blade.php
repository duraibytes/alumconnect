@extends( 'layouts.login_layout' )

@section('content')

<div class="login-signin">
    <div class="mb-20">
        <h3>Forgotten Password ?</h3>
        <p class="opacity-60">Enter your email to reset your password</p>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session( 'status' ) }}
        </div>
    @endif
    
    
    <form method="POST" action="{{ route('custom_password') }}" class="form">
        @csrf
        <div class="form-group mb-10">
            <input id="email" type="email" class="form-control  h-auto text-white placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus placeholder="Email">
            @if( $errors->any() )
                @foreach( $errors->all() as $key => $value )
                    <span class="text-danger" role="alert">
                        <strong>{{ $value }}</strong>
                    </span>  
                @endforeach
            @endif
            
            {{-- @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror --}}
        </div>
        <div class="form-group">
            <button class="btn btn-pill btn-outline-white font-weight-bold opacity-90 px-15 py-3 m-2" type="submit">
                Request
            </button>
            <a href="{{ route( 'login' ) }}" class="btn btn-pill btn-outline-white font-weight-bold opacity-70 px-15 py-3 m-2">
                Go back
            </a>
        </div>
    </form>
</div>

@endsection
