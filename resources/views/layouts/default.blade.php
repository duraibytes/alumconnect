<!DOCTYPE html>
<html lang="en">

@include( 'layouts.parts.header' )
 
<body id="kt_body" style="background-image: url( {{ asset( 'images/bg/bg-10.jpg' ) }} )" class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">
    <!--begin::Header Mobile-->
    @include( 'layouts.parts._header-mobile' )
    <!--end::Header Mobile-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                <!--begin::Header-->
                @include( 'layouts.menu.header' )
                <!--end::Header-->
    
                {{-- MAIN CONTENT COMES HERE --}}
                @yield( 'content' )
                {{-- MAIN CONTENT ENDS COMES HERE --}}

                @include( 'layouts.parts.footer' )
                @include( 'layouts.parts.script' )

            </div>
        </div>
    </div>
    {{-- implement here modeal --}}
    @include( 'layouts.modal.user_side_modal' )
    {{-- include modal here --}}
    <div class="modal fade" id="baseModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    </div>
    <div class="modal" id="firstModal" data-backdrop="static" data-keyboard="false" style="z-index: 9990;">
    </div>
    <div class="modal fade bs-modal-sm" id="are_you_sure" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div style="display: block; font-weight: bold;" class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <div class="modal-title"></div>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
</body>

</html>

