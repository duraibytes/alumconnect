<!DOCTYPE html>
<html lang="en">

@include( 'layouts.parts.header' )
<link href="{{ asset('css/plugins/login-3.css') }}" rel="stylesheet" type="text/css" />
    
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-3 login-signin-on d-flex flex-row-fluid" id="kt_login">
            <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid" style="background-image: url( {{ config( 'global.LOGIN_PAGE_BG' ) }} );">
                <div class="login-form text-center text-white p-7 position-relative overflow-hidden">
                    <!--begin::Login Header-->
                    <div class="d-flex flex-center mb-15">
                        <a href="#">
                            <img src="{{ config('global.LOGO_URL_ICON') }}" class="max-h-100px" alt="" />
                        </a>
                    </div>
                    <!--end::Login Header-->
                    @yield( 'content' )
                </div>
            </div>
        </div>
        <!--end::Login-->
    </div>
    
@include( 'layouts.parts.script' )
<script src="{{ asset( 'js/login-general.js' ) }}"></script>
</body>

</html>

