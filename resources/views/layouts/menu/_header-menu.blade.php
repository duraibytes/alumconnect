<!--begin::Header Menu Wrapper-->
@php
    $routeName                  = Route::currentRouteName();
    // echo $routeName;
@endphp
<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
    <!--begin::Header Menu-->
    <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
        <!--begin::Header Nav-->
        <ul class="menu-nav">
            <li class="menu-item  menu-item-submenu menu-item-rel @if( $routeName == 'home' ) menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route( 'home' ) }}" class="menu-link">
                    <span class="menu-text">Dashboard</span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            
            <li class="menu-item menu-item-submenu @if( $routeName == 'post' || $routeName == 'blog'  ) menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route( 'post' ) }}" class="menu-link">
                    <span class="menu-text"> Post </span>
                    <span class="menu-desc"></span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="menu-item menu-item-submenu @if( $routeName == 'broadcast' ) menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route( 'broadcast' )}}" class="menu-link">
                    <span class="menu-text"> Broadcast </span>
                    <span class="menu-desc"></span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="menu-item menu-item-submenu @if( $routeName == 'students' ) menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route( 'students' )}}" class="menu-link">
                    <span class="menu-text"> Students </span>
                    <span class="menu-desc"></span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="menu-item menu-item-submenu @if( $routeName == 'referrals' ) menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route( 'referrals' )}}" class="menu-link">
                    <span class="menu-text"> Referrals </span>
                    <span class="menu-desc"></span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="menu-item menu-item-submenu @if( $routeName == 'users.index' ) menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
                <a href="{{ route( 'users.index' ) }}" class="menu-link">
                    <span class="menu-text"> User </span>
                    
                </a>
              
            </li>
            <li class="menu-item menu-item-submenu menu-item-rel menu-item-open-dropdown " data-menu-toggle="click" aria-haspopup="true">
                <a href="javascript:;" class="menu-link menu-toggle">
                    <span class="menu-text"> Settings </span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="menu-submenu menu-submenu-classic menu-submenu-left" data-hor-direction="menu-submenu-left">
                    <ul class="menu-subnav">
                        <li class="menu-item menu-item-active" aria-haspopup="true">
                            <a href="{{ route( 'category' ) }}" class="menu-link">
                                <span class="menu-text"> Categories </span>
                                <span class="menu-desc"></span>
                            </a>
                        </li>
                        <li class="menu-item" aria-haspopup="true">
                            <a href="{{ route( 'myaccount' ) }}" class="menu-link">
                                <span class="menu-text"> My account </span>
                                <span class="menu-desc"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--end::Header Nav-->
    </div>
    <!--end::Header Menu-->
</div>
<!--end::Header Menu Wrapper-->
