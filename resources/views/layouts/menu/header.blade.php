<div id="kt_header" class="header header-fixed">
    <!--begin::Container-->
    <div class="container d-flex align-items-stretch justify-content-between">
        <!--begin::Left-->
        <div class="d-flex align-items-stretch mr-3">
            <!--begin::Header Logo-->
            <div class="header-logo">
                <a href="javascript:;">
                    <img alt="Logo" src="{{ config('global.LOGO_URL_ICON') }}" class="logo-default max-h-40px" />
                    <img alt="Logo" src="{{ config('global.LOGO_URL_ICON') }}" class="logo-sticky max-h-40px" />
                </a>
            </div>
            <!--end::Header Logo-->
            <!--begin::Header Menu Wrapper-->
            @include( 'layouts.menu._header-menu' )
            <!--end::Header Menu Wrapper-->
        </div>
        <!--end::Left-->
        <!--begin::Topbar-->
        @php
        use App\Models\User;
            $user_id                        = Auth::user()->id;
            $user_info                      = User::with( 'category_info' )->find( $user_id );
        @endphp
        <div class="topbar">
            <!--begin::User-->
            <div class="dropdown">
                <!--begin::Toggle-->
                <div class="topbar-item">
                    <div class="btn btn-icon btn-hover-transparent-white d-flex align-items-center btn-lg px-md-2 w-md-auto" id="kt_quick_user_toggle">
                        <span class="symbol symbol-35">
                            <span class="symbol-label text-white font-size-h5 font-weight-bold bg-white-o-30">
                                {{-- {{ strtoupper( Auth::user()->name[0] ) }}  --}}
                                <div class="symbol-label" style="background-image:url('{{ $user_info->profile_photo }}')"></div>
                            </span>
                        </span>
                        <span class="text-white opacity-90 font-weight-bolder font-size-base d-none d-md-inline mr-4 ml-3"> {{ Auth::user()->name }} </span>
                        
                    </div>
                </div>
                <!--end::Toggle-->
            </div>
            <!--end::User-->
       
            <!--begin::User-->
            <div class="dropdown">
                <!--begin::Toggle-->
                <div class="topbar-item">
                    <div class="btn btn-icon btn-hover-transparent-white d-flex align-items-center btn-lg px-md-2 w-md-auto" >
                        <a href="{{ route( 'logout' ) }}">
                            <span class="symbol bg-white-o-30 p-2">
                                <span class="text-white opacity-90 font-weight-bolder font-size-base d-none d-md-inline"> Logout </span>
                            </span>
                        </a>
                    </div>
                </div>
                <!--end::Toggle-->
            </div>
            <!--end::User-->
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Container-->
</div>

