<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name') }}@if( isset( $title ) )@yield( 'title', ( ( $title ) ? ' - '.$title : '' )  ) @endif </title>
    <meta name="description" content="Updates and statistics" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="canonical" href="https://keenthemes.com/metronic" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
   
    <link href="{{ asset('css/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
   
    <link href="{{ asset('css/plugins/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/plugins/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/plugins/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    
    <link rel="shortcut icon" href="{{ asset( 'images/logo/favicon.ico' )}}" />
    {{-- add on css for all pages will include here --}}
    @yield( 'add_on_css' )
    <style>
        .table-striped tbody tr:nth-of-type(odd) {
            background-color: #ebedf361 !important;
        }

        #spinner {
            position: fixed;
            top: 50%;
            left: 50%;
        }

        .paginate_button:active, .paginate_button.active {
            background: #6993ff !important;
        }
        .paginate_button > a {
            color: white !important;
        }
        
    </style>
    <script>
    window.Userback = window.Userback || {};
    Userback.access_token = '6167|24280|grguUUErXIZWM8udBRtd4sEZ5aPaGYAxtuTCjVtZwCcmOrSeiZ';
    (function(d) {
        var s = d.createElement('script');s.async = true;
        s.src = 'https://static.userback.io/widget/v1.js';
        (d.head || d.body).appendChild(s);
    })(document);
</script>
</head>