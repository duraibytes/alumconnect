<!--end::Demo Panel-->
<script>var HOST_URL = "/metronic/theme/html/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
  
<script src="{{ asset( 'js/plugins/plugins.bundle.js' ) }}"></script>
<script src="{{ asset( 'js/plugins/prismjs.bundle.js' ) }}"></script>
<script src="{{ asset( 'js/plugins/scripts.bundle.js' ) }}"></script>
<script src="{{ asset( 'js/plugins/toastr.js' ) }}"></script>

<!--end::Global Theme Bundle-->
<!--begin::Page Vendors(used by this page)-->
<script src="{{ asset( 'js/plugins/fullcalendar.bundle.js' ) }}"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="{{ asset( 'js/pages/widgets.js' ) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
{{-- <script src="{{ asset( 'js/plugins/summer_note.js' ) }}"></script> --}}
<script src="{{ asset( 'js/pages/charts/apexcharts.js' ) }}"></script>
<script>
  function printErrorMsg (msg) {
    toastr.options = {
        "closeButton": true,
        "debug": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
        var err_msg         = '';
        $.each( msg, function( key, value ) {
            console.log( value );
            err_msg += '<p>'+value+'</p>';
        });
        toastr.error( err_msg );
    }
  function are_you_sure( function_name, title, message = 'Are you sure want to delete?', arg, arg_2, arg_3 ) {
    
      if ( typeof arg === 'undefined' ) {
          var arg = '';
      }
      if ( arg_2 === undefined ) {
          var arg_2   = '';
      }
      if ( arg_3 === undefined ) {
          var arg_3   = '';
      }

      var continue_proceess = 0;
      $( '#are_you_sure' ).modal();
      $( '#are_you_sure .modal-body' ).html( '<div align="center"><p>' + message + '</p></div>' );

      $( '#are_you_sure .modal-body' ).append( '<br /><br /><div align="center"><button class="btn btn-info btn-xs" id="continuemodal" data-dismiss="modal">Yes</button>&nbsp;&nbsp;<button aria-hidden="true" data-dismiss="modal" class="btn btn-danger btn-xs">Cancel</button></div>' );
      $( '#are_you_sure .modal-title' ).text( title );
      $( '#continuemodal' ).click( function() {
          if( arg_2 == '' && arg_3 == '' ) {
              eval( function_name + '(' + arg +')' );
          } else if ( arg_2 != '' && arg_3 == '' ) {
              eval( function_name + '(' + arg +', \'' + arg_2 + '\' )' );
          } else {
              eval( function_name + '(' + arg +', \'' + arg_2 + '\', \'' + arg_3 + '\' )' );
          }
      });
      return false;
  }

  //dashboard gerneral function for all pages
  function open_add_blog_form() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "post.blog_add" )}}',

                success  : function ( data ) {
                    
                    $( '#firstModal' ).modal();  
                    $( '#firstModal' ).html( data );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

        function insert_post() {

            var form_data           = new FormData( $( '#post_form' )[0] );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type                : 'POST',
                url                 : '{{route( "post.insert" )}}',
                processData         : false,
                contentType         : false,
                data                : form_data,
                dataType            : 'json',
                beforeSend          : function(){
                    $( "#save_button" ).addClass( "spinner spinner-right" );
                },
                success             : function ( data ) {
                    if( data.success ) {
                        toastr.success( data.success );
                        setTimeout( function() {
                            if( data.url ) {
                                console.log( data.url );
                                window.location.replace(data.url);
                            }
                            
                        }, 1500 );
                    } else if( data.failure ) {
                        
                        printErrorMsg( data.failure );
                        $( "#save_button" ).removeClass( "spinner spinner-right" );
                    }
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
            }
    
        function open_add_video_form() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "post.video_add" )}}',

                success  : function ( data ) {
                    
                    $( '#firstModal' ).modal();  
                    $( '#firstModal' ).html( data );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }
        // user functions
        function open_add_user_form() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "users.add" )}}',

                success  : function ( data ) {
                    
                    $( '#firstModal' ).modal();  
                    $( '#firstModal' ).html( data );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

        function insert_user() {

            var form_data           = new FormData( $( '#user_form' )[0] );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type                : 'POST',
                url                 : '{{route( "users.insert" )}}',
                processData         : false,
                contentType         : false,
                data                : form_data,
                dataType            : 'json',
                beforeSend          : function(){
                    $( "#save_button" ).addClass( "spinner spinner-right" );
                },
                success             : function ( data ) {
                    if( data.success ) {
                        toastr.success( data.success );

                        setTimeout( function() {
                            window.location.href = "{{route( 'users.index' )}}";
                        }, 1500 );
                    } else if( data.failure ) {
                        
                        printErrorMsg( data.failure );
                        $( "#save_button" ).removeClass( "spinner spinner-right" );
                    }
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

</script>
@yield( 'add_on_script' )
{{-- incud modals for all pages --}}
