<div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Heading-->
            <div class="d-flex flex-column">
                <!--begin::Title-->
                <h2 class="text-white font-weight-bold my-2 mr-5">{{ ucfirst( $title ) }}</h2>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <div class="d-flex align-items-center font-weight-bold my-2">
                    <!--begin::Item-->
                    <a href="#" class="opacity-75 hover-opacity-100">
                        <i class="flaticon2-shelter text-white icon-1x"></i>
                    </a>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>
                    <a href="" class="text-white text-hover-white opacity-75 hover-opacity-100">{{ ucfirst( $title ) }}</a>
                    <!--end::Item-->
                </div>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Heading-->
        </div>
        <!--end::Info-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            @if( isset( $action[ 'is_drop_menu' ] ) && $action[ 'is_drop_menu' ] == 'yes' ) 
            <!--begin::Dropdown-->
            <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="top">
                <a href="#" class="btn btn-white font-weight-bold py-3 px-6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</a>
                <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
                    <!--begin::Navigation-->
                    <ul class="navi navi-hover py-5">
                        <li class="navi-item">
                            <a href="#" onclick="return open_add_blog_form()" class="navi-link">
                                <span class="navi-icon">
                                    <i class="flaticon2-plus-1"></i>
                                </span>
                                <span class="navi-text"> Blog </span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" onclick="return open_add_video_form()" class="navi-link">
                                <span class="navi-icon">
                                    <i class="flaticon2-plus-1"></i>
                                </span>
                                <span class="navi-text"> Video </span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" onclick="return open_add_user_form()" class="navi-link">
                                <span class="navi-icon">
                                    <i class="flaticon2-plus-1"></i>
                                </span>
                                <span class="navi-text"> User </span>
                            </a>
                        </li>
                        
                        <li class="navi-separator my-3"></li>
                        <li class="navi-item">
                            <a href="{{ route( 'referrals' ) }}" class="navi-link">
                                <span class="navi-icon">
                                    <i class="flaticon2-gear"></i>
                                </span>
                                <span class="navi-text">Referrals</span>
                            </a>
                        </li>
                        
                    </ul>
                    <!--end::Navigation-->
                </div>
            </div>
            @endif
            @if( isset( $action[ 'is_drop_menu' ] ) && $action[ 'is_drop_menu' ] == 'no' ) 
                <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="{{ $action[ 'tooltip' ] }}" data-placement="top">
                    <a href="javascript:;" onclick="{{$action[ 'btn_link']}}" class="btn btn-white font-weight-bold py-3 px-6" aria-haspopup="true" aria-expanded="false">
                        <span class="navi-icon">
                            <i class="flaticon2-plus-1"></i>
                        </span> {{ $action[ 'btn_name' ]  }}
                    </a>
                </div>
            @endif
            <!--end::Dropdown-->
        </div>
        <!--end::Toolbar-->
    </div>
</div>