
@include('mail.header_section' )
    @include('mail._header_mail')
        
        <tr>
            <td class="body" width="100%" cellpadding="0" cellspacing="0">
                <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
        
                    <tr>
                        <td class="content-cell">

                            <div>Hello {{$student_info->name}},</div>
                            <div>Your {{config('app.name')}} account has been approved. <a href="{{ url('/login') }}">Log in here</a>.</div>
                            <br />
                            <div>Please contact {{config( 'constants.SUPPORT_EMAIL' )}} if any questions or concerns.</div>
                            <br />
                            <div>Best wishes,</div>
                            <div>{{config('app.name')}} Team</div>
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
@include('mail.footer_section')

                   