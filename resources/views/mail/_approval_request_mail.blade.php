
@include('mail.header_section' )
    @include('mail._header_mail')
        
        <tr>
            <td class="body" width="100%" cellpadding="0" cellspacing="0">
                <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
        
                    <tr>
                        <td class="content-cell">

                            <div>Hello,</div>
                            <div>The following student has signed-up on the {{config('app.name')}} and is pending approval:</div>
                            <br />
                            <div>{{$student_info->name}}</div>
                            <div>{{$student_info->email}}</div>
                            @if($student_info->mobile)   
                                <div>{{$student_info->mobile}}</div>
                            @endif
                            <div>TYPE: {{$student_info->category_info->name}}</div>
                            <div>Student ID: 
                                @if($student_info->category == 3)
                                    @if($student_info->student_id_file)
                                        Provided
                                    @else
                                        Not provided
                                    @endif
                                @else
                                    Not applicable
                                @endif
                            </div>
                            <br />
                            <div>Log in to Admin portal to verify the account. </div>
                            <div>Best wishes,</div>
                            <div>{{config('app.name')}} Team</div>
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
@include('mail.footer_section')

                   