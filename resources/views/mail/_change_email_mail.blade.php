
@include('mail.header_section' )
    @include('mail._header_mail')
        
        <tr>
            <td class="body" width="100%" cellpadding="0" cellspacing="0">
                <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
        
                    <tr>
                        <td class="content-cell">

                            <div>Hello from {{config('app.name')}}!</div>
                            <div>Please confirm your email address by clicking: <a href="{{ url('/api/confirm_email/' . $unique_key . '') }}"> here</a></div>
                            <br />
                            <div>If you have any issues, please feel free to email us at {{config( 'constants.SUPPORT_EMAIL' )}} </div>
                            <br />
                            <div>Best wishes,</div>
                            <div>{{config('app.name')}} Team</div>
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
@include('mail.footer_section')

                   