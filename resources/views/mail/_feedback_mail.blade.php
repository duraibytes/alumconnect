
@include('mail.header_section' )
    @include('mail._header_mail')
        
        <tr>
            <td class="body" width="100%" cellpadding="0" cellspacing="0">
                <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
        
                    <tr>
                        <td class="content-cell">

                            <div>Hello,</div>
                            <div>{{config('app.name')}} Feedback details :</div>
                            <br />
                            <div>{{$feedback_data[ 'name' ]}}</div>
                            <div>{{$feedback_data[ 'email' ]}}</div>
                            <div>{{$feedback_data[ 'message' ]}}</div>
                            <br />
                            <div>Best wishes,</div>
                            <div>{{config('app.name')}} Team</div>
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
@include('mail.footer_section')

                   