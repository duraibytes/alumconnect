
@include('mail.header_section' )
    
    <tr>
        <td style="text-align: left;padding:10px 27px 12px 40px;">
            <a href="{{ url('/') }}" style="display: inline-block;">
            
                <img src="{{ config('global.LOGO_URL') }}" class="logo" alt="{{ config( 'app.name' )}} Logo" style="height: auto;
                width: auto;">
        
            </a>
        </td>
    </tr>    
        <tr>
            <td style="background: white;padding-left:15px;padding-bottom:30px;" width="100%" cellpadding="0" cellspacing="0">
                <table align="left" width="570" cellpadding="0" cellspacing="0" role="presentation">
        
                    <tr>
                        <td class="content-cell">
                            <table style="width: 100%;">
                                <tr> 
                                    <th style="text-align:left;">
                                        Email:
                                    </th>
                                    <td style="text-align:left;">
                                        {{ $email }}
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align:left;width:100%"> Password: </th>
                                
                                    <td style="text-align: left;">
                                        {{ $password }}
                                    </td>
                                </tr>
                            </table>
                        
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 35px;">
                            <a href="{{ url('/') }}" style="color: #fff;
                background-color: #6993ff;
                border-color: #6993ff;
                cursor: pointer;
                outline: 0!important;
                vertical-align: middle;
                display: inline-block;
                font-weight: 400;
                border: 1px solid transparent;
                padding: .65rem 1rem;
                font-size: 1rem;
                line-height: 1.5;
                border-radius: .42rem;
                text-decoration: none;
                ">
                    Sign In
                </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 35px;">
                            Or click <a href="{{ url('/') }}"> here </a> to login
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;padding:20px;">
                @ ALUMCONNECT
            </td>
        </tr>
@include('mail.footer_section')

                   