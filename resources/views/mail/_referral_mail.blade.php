
@include('mail.header_section' )
    @include('mail._header_mail')
        
        <tr>
            <td class="body" width="100%" cellpadding="0" cellspacing="0">
                <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
        
                    <tr>
                        <td class="content-cell">

                            <div>Hello,</div>
                            <div>{{$referral_info[ 'referral_name' ]}} has referred you on the {{config('app.name')}}. Kindly use the below referral code while signing up.</div>
                            <br />
                            <div>Referral Code: {{$referral_info[ 'referral_code' ]}}</div>
                            <br />
                            <div>To signup on {{config('app.name')}} <a href="{{ url('/signup/') }}">Click here</a>.</div>
                            <br />
                            <div>Best wishes,</div>
                            <div>{{config('app.name')}} Team</div>
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
@include('mail.footer_section')

                   