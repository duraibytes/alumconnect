
@include('mail.header_section' )
@include('mail._header_mail')
    
    <tr>
        <td class="body" width="100%" cellpadding="0" cellspacing="0">
            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
    
                <tr>
                    <td class="content-cell">
                        <table style="width: 100%;">
                            <tr>
                                <th style="text-align:center;width:100%"> Verify Email </th>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    {{-- <a href="{{url('verify_email', $email_token ) }}">Verify Email</a> --}}
                                    <a href="{{ route( 'verify_email', [ 'email' => $email_token ] ) }}">
                                    Click To Verify </a>
                                    
                                </td>
                            </tr>
                        </table>
                    
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@include('mail.footer_section')

               