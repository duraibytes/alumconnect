
@php
use App\Models\Category;
    
@endphp
@if( isset( $info ) && !empty( $info ) )
    @foreach( $info as $key => $value )
        <div class="card p-5 main-cat">
            <h2 class="text-success text-center">
                {{ $value->type }}
            </h2>
        {{-- get individual data  --}}
            @php
                $items                           = Category::with( [ 'tag_list' ] )
                                                ->where( 'type', $value->type )
                                                ->get();
                
            @endphp
        @if( isset( $items ) && !empty( $items ) )
            @foreach( $items as $ikey => $ivalue )
                <div id="p-3">
                    <h3>{{ $ivalue->name }} &nbsp;
                        @if( $ivalue->page )
                        <span class="label label-warning label-pill label-inline mr-2">
                        {{ $ivalue->page }}
                        </span> 
                        @endif
                        <a href="javascript:;" onclick="return edit_category( '{{ $ivalue->id }}')">
                            <i class="flaticon2-pen"></i>
                        </a>
                    </h3>
                    @if( isset( $ivalue->tag_list[0] ) && !empty( $ivalue->tag_list[0] ) )
                        <div>
                            @foreach( $ivalue->tag_list as $tagkey => $tagvalue )
                                @if( $tagvalue )
                                    <span>
                                        <a href="javascript:;" class="label label-info label-inline mr-2" onclick="edit_tag( '{{$tagvalue->id}}' )" > {{ $tagvalue->name }}</a>
                                    </span>
                                    
                                @endif
                            @endforeach
                            <a href="javascript:;" onclick="add_tag( '{{ $ivalue->id }}' )"><i class="fa fa-plus"></i></a>
                        </div>
                    @endif
                </div>
                <hr>
            @endforeach
        @endif
        </div>
        
    @endforeach
@endif

@section( 'add_on_css' )
<style>
    .each-panel {
        padding: 10px;
        background: azure;
        border: 1px dashed lightgray;
    }
    .main-cat {
        background-image: linear-gradient(
179deg
, #9e9e9e00, #8ef2ff36, transparent);
    }
</style>

@endsection
    {{-- {{ SHOW tags FROM categories WHERE type = TAG AND parent_id = $category_id }}
    +<span class="label label-info label-inline mr-2" onclick=”edit_tag( $category_id )” >In process</span> ... <i icon-plus add_tag($parent_id) --}}