<div class="card card-custom">
    <!--begin::Form-->
    <div class="card-body">
        <div class="form-group mb-8 alert-default" style="display: none;">
            <div class="alert alert-custom alert-default" role="alert">
                <div class="alert-icon"><i class="flaticon-warning text-primary"></i></div>
                <div class="alert-text">
                    
                </div>
            </div>
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label> Title <span class="text-danger">*</span>
                </label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" value="{{ ( isset( $category_info->name ) && !empty( $category_info->name ) ) ? $category_info->name : '' }}"/>
            </div>
            
        </div>
    </div>
    <!--end::Form-->
</div>