<div class="modal-dialog  modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="firstModalLabel"> {{ $title }} </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>
        <div class="modal-body" data-scroll="true">
            <form id="category_form" >
                @csrf
                @include('pages.category.form._form' ) 
                <input type="hidden" name="id" value="{{ $id }}">
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary font-weight-bold" id="save_button" onclick="return update_category()">Save changes</button>
        </div>
    </div>
</div>


<script>
    function update_category() {

        var form_data           = new FormData( $( '#category_form' )[0] );
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
            }
        });
        $.ajax({
            type                : 'POST',
            url                 : '{{route( "category.update" )}}',
            processData         : false,
            contentType         : false,
            data                : form_data,
            dataType            : 'json',
            beforeSend          : function(){
                $( "#save_button" ).addClass( "spinner spinner-right" );
            },
            success             : function ( data ) {
                if( data.success ) {
                    toastr.success( data.success );
                    setTimeout( function() {
                        
                        window.location.href = "{{ route( 'category' )}}";
                        
                        
                    }, 1500 );
                } else if( data.failure ) {
                    
                    printErrorMsg( data.failure );
                    $( "#save_button" ).removeClass( "spinner spinner-right" );
                }
            },
            error: function ( data ) {
                console.log('Error:', data);
            }
        });
        return true;
    }
</script>
