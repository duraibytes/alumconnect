<div class="row">
   <div class="col-lg-12">
      <!--begin::Card-->
      <div class="card card-custom gutter-b">
         <div class="card-header">
            <div class="card-title">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
               <h3 class="card-label">Students Sign-up by Type</h3>
            </div>
         </div>
         <div class="card-body">
            <!--begin::Chart-->
            <div id="chart_3"></div>
            <!--end::Chart-->
         </div>
      </div>
      <!--end::Card-->
   </div>
</div>
<div class="row">
   <div class="col-xl-3">
      <div class="card card-custom gutter-b" style="height: 150px">
         <div class="card-body">
            <span class="svg-icon svg-icon-3x svg-icon-success">
               <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                     <polygon points="0 0 24 0 24 24 0 24" />
                     <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                     <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
               </svg>
               <!--end::Svg Icon-->
            </span>
            <div class="text-dark font-weight-bolder font-size-h2 mt-3">{{$no_verified_students}}</div>
            <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">No of (verified) Students</a>
         </div>
      </div>
   </div>
   <div class="col-xl-3">
      <div class="card card-custom gutter-b" style="height: 150px">
         <div class="card-body">
            <span class="svg-icon svg-icon-3x svg-icon-success">
               <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                     <polygon points="0 0 24 0 24 24 0 24" />
                     <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                     <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
               </svg>
               <!--end::Svg Icon-->
            </span>
            <div class="text-dark font-weight-bolder font-size-h2 mt-3">{{$no_referral_payout}}</div>
            <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">Referrals payout</a>
         </div>
      </div>
   </div>
   <div class="col-xl-3">
      <div class="card card-custom gutter-b" style="height: 150px">
         <div class="card-body">
            <span class="svg-icon svg-icon-3x svg-icon-success">
               <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                     <polygon points="0 0 24 0 24 24 0 24" />
                     <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                     <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
               </svg>
               <!--end::Svg Icon-->
            </span>
            <div class="text-dark font-weight-bolder font-size-h2 mt-3">{{$no_blogs}}</div>
            <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">No of Blogs</a>
         </div>
      </div>
   </div>
   <div class="col-xl-3">
      <div class="card card-custom gutter-b" style="height: 150px">
         <div class="card-body">
            <span class="svg-icon svg-icon-3x svg-icon-success">
               <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                     <polygon points="0 0 24 0 24 24 0 24" />
                     <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                     <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
               </svg>
               <!--end::Svg Icon-->
            </span>
            <div class="text-dark font-weight-bolder font-size-h2 mt-3">{{$no_broadcast}}</div>
            <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">No of Broadcast</a>
         </div>
      </div>
   </div>
</div>

               
<script>
    // get_list();

   var future_count  = JSON.parse( ( "{{ $future_count }}" ).replace(/&quot;/g,'"') ); 
   var alumni_count  = JSON.parse( ( "{{$alumni_count}}" ).replace(/&quot;/g,'"') );
   var current_count = JSON.parse( ( "{{$current_count}}" ).replace(/&quot;/g,'"') );
   var signup_months = JSON.parse( ( "{{$signup_months}}" ).replace(/&quot;/g,'"') );

   var KTApexChartsDemo = function () {
      // Private functions
      var _demo3 = function () {
         const apexChart = "#chart_3";
         var options = {
            series: [{
               name: 'Future',
               data: future_count
            }, {
               name: 'Alumni',
               data: alumni_count
            }, {
               name: 'Current',
               data: current_count
            }],
            chart: {
               type: 'bar',
               height: 350
            },
            plotOptions: {
               bar: {
                  horizontal: false,
                  columnWidth: '55%',
                  endingShape: 'rounded'
               },
            },
            dataLabels: {
               enabled: false
            },
            stroke: {
               show: true,
               width: 2,
               colors: ['transparent']
            },
            xaxis: {
               categories: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
            },
            yaxis: {
               title: {
                  text: 'Student(s)'
               }
            },
            fill: {
               opacity: 1
            },
            tooltip: {
               y: {
                  formatter: function (val) {
                     return "" + val + " Student(s)"
                  }
               }
            },
            colors: [primary, success, warning]
         };

         var chart = new ApexCharts(document.querySelector(apexChart), options);
         chart.render();
      }

      return {
         // public functions
         init: function () {
            _demo3();
         }
      };
   }();

    function get_list() {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
            type     : 'POST',
            url      : '{{ route( "sample.list" ) }}',
            success  : function ( data ) {
                $( '#kt_datatable' ).html( data );
            },
            error: function ( data ) {
                console.log('Error:', data);
            }
        });
        return true;
    }
</script>
   
