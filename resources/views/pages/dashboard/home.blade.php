@extends( 'layouts.default' )

@section( 'content' )
  <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        @include('layouts.parts.sub_header' )
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        @include( 'pages.dashboard._datatable_list' )
                    </div>
                </div>
            </div>
        </div>
    </div>
                        
@endsection



