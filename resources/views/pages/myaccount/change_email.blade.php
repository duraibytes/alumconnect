<div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="firstModalLabel"> Change Email </h5>
            <span class="form-text text-muted"> Requires email verification.  </span>
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>
        <div class="modal-body" data-scroll="true">
            <form id="email_form" >
                @csrf
                <div class="card card-custom">
                    <!--begin::Form-->
                    <div class="card-body">
                        
                        <div class="form-group">
                            <label> Email <span class="text-danger">*</span>
                            </label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Enter email"  />
                            <span class="form-text text-muted"> We'll never share your email with anyone else.</span>
                        </div>

                        <div class="form-group">
                            <label> Current Password <span class="text-danger">*</span>
                            </label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" />
                        </div>
                        <input type="hidden" name="id" value="{{ $id }}">
                    </div>
                    <!--end::Form-->
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary font-weight-bold" id="save_button"onclick="return send_change_email()">Save changes</button>
        </div>
    </div>
</div>