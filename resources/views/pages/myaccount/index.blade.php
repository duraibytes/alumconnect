@extends( 'layouts.default' )

@section( 'content' )
  <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
     
        @include('layouts.parts.sub_header' )
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom card-stretch" style="width: 500px;margin: 0 auto;height:auto"> 
                <!--begin::Body-->
                    <div class="card-body pt-15">
                                <!--begin::Toolbar-->
                                {{-- <div class="d-flex justify-content-end">
                                    <div class="dropdown dropdown-inline">
                                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ki ki-bold-more-hor"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="">
                                            <!--begin::Navigation-->
                                            {{-- @include('pages.myaccount._user_nav_menu') --}}
                                            <!--end::Navigation-->
                                        {{-- </div>
                                    </div>
                                </div> --}}
                                <!--end::Toolbar-->
                                <!--begin::User-->
                                <form id="photo_form" >
                                    @csrf
                                <div class="d-flex align-items-center">
                                    {{-- <div class="symbol symbol-60 symbol-xxl-150 mr-5 align-self-start align-self-xxl-center">
                                        <div class="symbol-label" style="background-image:url('/metronic/theme/html/demo2/dist/assets/media/users/300_21.jpg')"></div>
                                        <i class="symbol-badge bg-success"></i>
                                    </div> --}}
                                    <div class="image-input image-input-outline image-input-empty" id="kt_profile_avatar" style="background-image: url( {{ $user_info->profile_photo }})">
                                        <div class="image-input-wrapper" id="loading" style="background-image: none;"></div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg" class="profile_upload">
                                            <input type="hidden" name="profile_avatar_remove" value="1">
                                        </label>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="" data-original-title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="" data-original-title="Remove avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                        <i class="symbol-badge bg-success"></i>
                                    </div>
                                        <input type="hidden" name="id" value="{{ $user_info->id }}">
                                    <div style="padding: 24px;margin: 0 auto;">
                                        <a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">{{ $user_info->name.' '.$user_info->fname }}</a>
                                        <div class="text-muted">
                                            <span class="label label-lg font-weight-bold  label-light-info label-inline">
                                                {{ $user_info->category_info->name }}
                                            </span></div>
                                        <div class="mt-2">
                                            <a href="{{ route( 'password_reset_user', [ 'email' => $user_info->email ] )}}" class="btn btn-sm btn-primary font-weight-bold mr-2 py-2 px-3 px-xxl-5 my-1" onclick="return confirm( 'Are you sure')"> 
                                                {{-- <a href="#" class="btn btn-sm btn-primary font-weight-bold mr-2 py-2 px-3 px-xxl-5 my-1" onclick="return are_you_sure( 'reset_password_user', 'Reset Passsword', 'Are you sure want to reset password?', '{{$user_info->email}}' )">  --}}
                                                Reset Password 
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <!--end::User-->
                                <!--begin::Contact-->
                                <div class="py-9">
                                    <div class="d-flex align-items-center justify-content-between mb-2">
                                        <span class="font-weight-bold mr-2">Email:</span>
                                        <a href="#" class="text-muted text-hover-primary" onclick="return change_email( {{ $user_info->id }})">{{ $user_info->email }}</a>
                                    </div>
                                    @if( $user_info->last_login )
                                    <div class="d-flex align-items-center justify-content-between mb-2">
                                        <span class="font-weight-bold mr-2">Last Login:</span>
                                        <span class="text-muted">{{ date( 'Y-m-d H:i:s T', strtotime( $user_info->last_login ) ) }}</span>
                                    </div>
                                    @endif
                                    {{-- <div class="d-flex align-items-center justify-content-between">
                                        <span class="font-weight-bold mr-2">Location:</span>
                                        <span class="text-muted">Melbourne</span>
                                    </div> --}}
                                </div>
                                <!--end::Contact-->
                                <!--begin::Nav-->
                               
                                <!--end::Nav-->
                            </div>
                            <!--end::Body-->
                        </div>
                    
            </div>
        </div>
    </div>
                        
@endsection

@section( 'add_on_css' )
    <style>
        .image-input-wrapper {
            width: 200px !important;
            height: 200px !important;
        }
    </style>
@endsection

@section( 'add_on_script' )
    <script>
        $( ".profile_upload" ).on( 'change', function() {
            var form_data           = new FormData( $( '#photo_form' )[0] );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type                : 'POST',
                url                 : '{{route( "profilephoto.change" )}}',
                processData         : false,
                contentType         : false,
                data                : form_data,
                dataType            : 'json',
                beforeSend          : function(){
                    $( "#loading" ).addClass( "spinner spinner-center" );
                },
                success             : function ( data ) {
                    if( data.success ) {
                        toastr.success( data.success );
                        setTimeout( function() {
                            window.location.href = "{{route( 'myaccount' )}}";
                        }, 1500 );
                    } else if( data.failure ) {
                        toastr.error( data.failure );
                        $( "#loading" ).removeClass( "spinner spinner-center" );
                    }
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        });

        function change_email( id ) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "change_email" )}}',
                data     : { id:id },
                success  : function ( data ) {
                    
                    $( '#firstModal' ).modal();  
                    $( '#firstModal' ).html( data );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

        function send_change_email() {

            var form_data           = new FormData( $( '#email_form' )[0] );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type                : 'POST',
                url                 : '{{route( "email.change" )}}',
                processData         : false,
                contentType         : false,
                data                : form_data,
                dataType            : 'json',
                beforeSend          : function(){
                    $( "#save_button" ).addClass( "spinner spinner-right" );
                },
                success             : function ( data ) {
                    if( data.success ) {
                        toastr.success( data.success );
                        setTimeout( function() {
                            window.location.href = "{{route( 'myaccount' )}}";
                        }, 1500 );
                    } else if( data.failure ) {
                        toastr.error( data.failure );
                        $( "#save_button" ).removeClass( "spinner spinner-right" );
                    }
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }
    </script>

@endsection