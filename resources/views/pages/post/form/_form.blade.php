<div class="card card-custom">
    <!--begin::Form-->
    <div class="card-body">
        <div class="form-group mb-8 alert-default" style="display: none;">
            <div class="alert alert-custom alert-default" role="alert">
                <div class="alert-icon"><i class="flaticon-warning text-primary"></i></div>
                <div class="alert-text">
                    
                </div>
            </div>
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label> Title <span class="text-danger">*</span>
                </label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Enter title" value="{{ ( isset( $post_info->title ) && !empty( $post_info->title ) ) ? $post_info->title : '' }}"/>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="exampleSelect1"> Category <span class="text-danger">*</span></label>
                <select class="form-control" id="exampleSelect1" name="category_id">
                    <option value=""> -- select category </option>
                    @if( $category ) 
                        @foreach( $category as $key => $value )
                            @php
                                $selected           = '';
                                if( isset( $post_info->category_id ) && $post_info->category_id == $value->id ) {
                                    $selected       = 'selected';
                                }
                            @endphp
                            <option value="{{ $value->id }}" {{$selected}}> {{ $value->name }}</option>    
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        @if( $type == 'VIDEO' )
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <label> Youtube Url <span class="text-danger">*</span>
                    </label>
                    <input type="text" class="form-control" name="url" id="url" placeholder="Enter title" value="{{ ( isset( $post_info->video_url ) && !empty( $post_info->video_url ) ) ? $post_info->video_url : '' }}"/>
                </div>
            </div>
        @endif
            
    </div>
    <!--end::Form-->
</div>