@extends( 'layouts.default' )

@section( 'content' )
  <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
     
        @include('layouts.parts.sub_header' )
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <form id="post_form">
                            
                            <div class="card card-custom">
                                <!--begin::Form-->
                                <div class="card-body">
                                    <div class="form-group mb-8 alert-default" style="display: none;">
                                        <div class="alert alert-custom alert-default" role="alert">
                                            <div class="alert-icon"><i class="flaticon-warning text-primary"></i></div>
                                            <div class="alert-text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label> Title <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter title" value="{{ ( isset( $post_info->title ) && !empty( $post_info->title ) ) ? $post_info->title : '' }}"/>
                                            </div>
                                        </div>
                                        @if( isset( $type ) && $type == 'BROADCAST' )
                                            <div class="col-sm-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label> Youtube Url <span class="text-danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control" name="url" id="url" placeholder="Enter title" value="{{ ( isset( $post_info->video_url ) && !empty( $post_info->video_url ) ) ? $post_info->video_url : '' }}"/>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label> Image 
                                                </label>
                                                <input type="file" class="form-control" name="image" id="image" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            @if( isset( $post_info->post_image ) && !empty( $post_info->post_image ) )
                                                <img src="{{ $post_info->post_image }}" alt="{{ $post_info->post_image }}" style="width:120px;">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type="hidden" name="id" value="{{ $id }}">
                                        <input type="hidden" name="type" value="{{ $type }}">
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="exampleSelect1"> Category </label>
                                                <select class="form-control" id="exampleSelect1" name="category_id">
                                                    <option value=""> -- select category </option>
                                                    @if( $category ) 
                                                        @foreach( $category as $key => $value )
                                                            @php
                                                                $selected           = '';
                                                                if( isset( $post_info->category_id ) && $post_info->category_id == $value->id ) {
                                                                    $selected       = 'selected';
                                                                }
                                                            @endphp
                                                            <option value="{{ $value->id }}" {{$selected}}> {{ $value->name }}</option>    
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="sub_category_id"> Tag </label>
                                                <select class="form-control" id="sub_category_id" name="sub_category_id">
                                                    <option value=""> -- select Tag -- </option>
                                                    @if( $tag_info ) 
                                                        @foreach( $tag_info as $key => $value )
                                                            @php
                                                                $selected           = '';
                                                                if( isset( $post_info->sub_category_id ) && $post_info->sub_category_id == $value->id ) {
                                                                    $selected       = 'selected';
                                                                }
                                                            @endphp
                                                            <option value="{{ $value->id }}" {{$selected}}> {{ $value->name }}</option>    
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label> Description 
                                                </label>
                                                <textarea class="form-control" id="description" name="description">@if( isset( $post_info ) ) {{$post_info->description}} @endif</textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="status"> Status </label>
                                                <select class="form-control" id="status" name="status">
                                                    <option value=""> -- select status </option>
                                                    <option value="DRAFT" @if( isset( $post_info->status ) && $post_info->status == 'DRAFT' ) selected @endif>DRAFT</option>
                                                    <option value="PUBLISHED" @if( isset( $post_info->status ) && $post_info->status == 'PUBLISHED' ) selected @endif>PUBLISHED</option>
                                                    <option value="ARCHIVED" @if( isset( $post_info->status ) && $post_info->status == 'ARCHIVED' ) selected @endif>ARCHIVED</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- end row --}}
                                        
                                </div>
                                <div class="card-footer text-right">
                                    <a href="{{ route( 'post' ) }}" class="btn btn-light-primary font-weight-bold" > Cancel </a>
                                    <button type="button" class="btn btn-primary font-weight-bold" id="save_button" onclick="return update_post()">Save changes</button>
                                </div>
                                <!--end::Form-->
                            </div>
                            
                            
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
                        
@endsection
@section( 'add_on_script' )
    <!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $( '#description' ).summernote({
         height:300,
     });
        function update_post() {

            var form_data           = new FormData( $( '#post_form' )[0] );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type                : 'POST',
                url                 : '{{route( "post.update" )}}',
                processData         : false,
                contentType         : false,
                data                : form_data,
                dataType            : 'json',
                beforeSend          : function(){
                    $( "#save_button" ).addClass( "spinner spinner-right" );
                },
                success             : function ( data ) {
                    if( data.success ) {
                        toastr.success( data.success );
                        setTimeout( function() {
                            if( data.type == 'BROADCAST' ){
                                window.location.href = "{{ route( 'broadcast' )}}";
                            } else {
                                window.location.href = "{{ route( 'post' )}}";
                            }
                            
                        }, 1500 );
                    } else if( data.failure ) {
                        
                        printErrorMsg( data.failure );
                        $( "#save_button" ).removeClass( "spinner spinner-right" );
                    }
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

       

    </script>
@endsection