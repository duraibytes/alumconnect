@extends( 'layouts.default' )

@section( 'content' )
  <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
     
        @include('layouts.parts.sub_header' )
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div id="success-msg"></div>
                        @if( $title == 'Post' )
                            @include( 'pages.post._blog_list' )
                        @elseif( $title == 'Broadcast' )
                            @include( 'pages.post._video_list' )
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
                        
@endsection

@section( 'add_on_css' )
<link rel="stylesheet" href="{{ asset('//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css') }}">
<style>
    #example_wrapper>.row, #video_table_wrapper>.row {
        width: 100% !important;
    }
    .dataTables_wrapper .dataTable td.orting_asc_disabled:after, .dataTables_wrapper .dataTable td.orting_asc_disabled:before, .dataTables_wrapper .dataTable td.orting_desc_disabled:after, .dataTables_wrapper .dataTable td.orting_desc_disabled:before, .dataTables_wrapper .dataTable td.sorting:after, .dataTables_wrapper .dataTable td.sorting:before, .dataTables_wrapper .dataTable td.sorting_asc:after, .dataTables_wrapper .dataTable td.sorting_asc:before, .dataTables_wrapper .dataTable td.sorting_desc:after, .dataTables_wrapper .dataTable td.sorting_desc:before, .dataTables_wrapper .dataTable th.orting_asc_disabled:after, .dataTables_wrapper .dataTable th.orting_asc_disabled:before, .dataTables_wrapper .dataTable th.orting_desc_disabled:after, .dataTables_wrapper .dataTable th.orting_desc_disabled:before, .dataTables_wrapper .dataTable th.sorting:after, .dataTables_wrapper .dataTable th.sorting:before, .dataTables_wrapper .dataTable th.sorting_asc:after, .dataTables_wrapper .dataTable th.sorting_asc:before, .dataTables_wrapper .dataTable th.sorting_desc:after, .dataTables_wrapper .dataTable th.sorting_desc:before {
       display: none !important;
    }
</style>
@endsection

@section( 'add_on_script' )

    <script src="{{ asset('https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset( 'js/pages/html-table.js' ) }}"></script>
    <script>
        $("#kt_datatable").KTDatatable();
        $(document).ready(function() {
            $( '#example' ).DataTable({
                columnDefs: [ {
                    'targets': [ 1, 6 ], /* column index [0,1,2,3]*/
                    'orderable': false, /* true or false */
                }],
                pageLength: 50,
                "language": {
                    "paginate": {
                    "previous": "<i class='ki ki-arrow-back'></i>",
                    "next": "<i class='ki ki-arrow-next'></i>"
                    }
                }
            });
            $( '#video_table' ).DataTable({
                columnDefs: [ {
                    'targets': [ 5 ], /* column index [0,1,2,3]*/
                    'orderable': false, /* true or false */
                }],
                pageLength: 50,
                "language": {
                    "paginate": {
                    "previous": "<i class='ki ki-arrow-back'></i>",
                    "next": "<i class='ki ki-arrow-next'></i>"
                    }
                }
            });
        });

        

        

        function delete_post( id ) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "post.delete_post" )}}',
                data     : { 'id' : id, _method:'DELETE' },
                success  : function ( data ) {
                    toastr.success( data.success );
                    setTimeout( function() {
                        if( data.type == 'BLOG' ) {
                            window.location.href = "{{route( 'post' )}}";
                        } else {
                            window.location.href = "{{route( 'broadcast' )}}";
                        }
                    }, 1500 );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

        

    </script>
@endsection