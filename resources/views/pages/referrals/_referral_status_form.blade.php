<div class="card card-custom">
    <!--begin::Form-->
    <div class="card-body">
        <div class="form-group mb-8 alert-default" style="display: none;">
            <div class="alert alert-custom alert-default" role="alert">
                <div class="alert-icon"><i class="flaticon-warning text-primary"></i></div>
                <div class="alert-text">
                    
                </div>
            </div>
        </div>
        
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label> Referral Status <span class="text-danger">*</span></label>
                <select class="form-control" name="referral_status" id="referral_status" placeholder="Select referral status">
                    <option value="">Select Referral Status</option>
                    @foreach ($referral_status as $key => $status)
                        <option {{ ( ( $student_info->referral_status == $status->name ) ? "selected" : "" ) }} value="{{$status->name}}">{{str_replace( "_", " ", $status->name )}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>