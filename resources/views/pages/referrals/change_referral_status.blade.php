<div class="modal-dialog  modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="firstModalLabel"> {{ $title }} </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>
        <div class="modal-body" data-scroll="true">
            <form id="referral_status_form" >
                @csrf
                @include( 'pages.referrals._referral_status_form' ) 
                <input type="hidden" name="id" value="{{ $id }}">
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary font-weight-bold" id="save_button" onclick="return update_referral_status()">Save changes</button>
        </div>
    </div>
</div>


<script>
    function update_referral_status() {

        var form_data           = new FormData( $( '#referral_status_form' )[0] );
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
            }
        });
        $.ajax({
            type                : 'POST',
            url                 : '{{route( "referrals.update_referral_status" )}}',
            processData         : false,
            contentType         : false,
            data                : form_data,
            dataType            : 'json',
            beforeSend          : function(){
                $( "#save_button" ).addClass( "spinner spinner-right" );
            },
            success             : function ( data ) {
                if( data.success ) {
                    toastr.success( data.success );
                    setTimeout( function() {
                        window.location.href = "{{ route( 'referrals' )}}";
                    }, 1500 );
                } else if( data.failure ) {
                    printErrorMsg( data.failure );
                    $( "#save_button" ).removeClass( "spinner spinner-right" );
                }
            },
            error: function ( data ) {
                console.log('Error:', data);
            }
        });
        return true;
    }
</script>
