<table class="table table-bordered">
    <tbody>
        <tr>
            <th>Name</th>
            <td>{{$student_info->name}}</td>
        </tr>
        @if( !empty( $student_info->category_info ) ) 
        <tr>
            <th>Category</th>
            <td>{{$student_info->category_info->name}} </td>
        </tr>
        @endif
        @if( !empty( $student_info->country_info ) )
        <tr>
            <th>Country</th>
            <td>{{$student_info->country_info->name}}</td>
        </tr>
        @endif
        @if( !empty( $student_info->mobile ) )
        <tr>
            <th>Mobile</th>
            <td>{{$student_info->mobile}}</td>
        </tr>
        @endif
        <tr>
            <th>Email</th>
            <td>{{$student_info->email}}</td>
        </tr>
    </tbody>
</table>