@extends( 'layouts.default' )

@section( 'content' )
  <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
     
        @include('layouts.parts.sub_header' )
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">

                <div class="card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col">
                                <label><strong>User Type :</strong></label>
                                <select name="filter_user_type" id='filter_user_type' class="form-control">
                                    <option value="">All User Type</option>
                                    @foreach( $user_types as $key => $type )
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col">
                                <label><strong>Country :</strong></label>
                                <select name="filter_country" id='filter_country' class="form-control">
                                    <option value="">All Country</option>
                                    @foreach( $countries_list as $key => $country )
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col">
                                <label><strong>Study Area :</strong></label>
                                <select name="filter_study_area" id='filter_study_area' class="form-control">
                                    <option value="">All Study Area</option>
                                    @foreach ($study_area as $key => $value)
                                        <option value="{{$value->id}}">{{str_replace( "_", " ", $value->name )}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col">
                                <label><strong>Language :</strong></label>
                                <select name="filter_language" id='filter_language' class="form-control">
                                    <option value="">All Language</option>
                                    @foreach ( config( 'constants.LANGUAGES' ) as $key => $value)
                                        <option value="{{$value['name']}}">{{$value['name']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col">
                                <label><strong>Intake :</strong></label>
                                <select name="filter_intake" id='filter_intake' class="form-control">
                                    <option value="">All Intake</option>
                                    @foreach ( config( 'constants.INTAKE' ) as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col">
                                <label><strong>Referral Status :</strong></label>
                                <select name="filter_referral_status" id='filter_referral_status' class="form-control">
                                    <option value="">All Referral Status</option>
                                    @foreach ($referral_status as $key => $status)
                                        <option value="{{$status->id}}">{{str_replace( "_", " ", $status->name )}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div class="excel_class">
                            <a href="{{ route( 'export' ) }}" class="btn btn-primary text-right"> Excel </a>
                        </div>
                        {{-- @include( 'pages.dashboard._datatable_list' ) --}}
                        <table id="stud-table" class="table table-bordered table-hover">
                            <thead class="datatable-head">
                                <tr class="datatable-row">
                                    <th data-field="RecordID" class="datatable-cell-left datatable-cell datatable-cell-sort datatable-cell-sorted" data-sort="asc"><span style="width: 10px;">#</span></th>
                                    <th data-field="OrderID" class="datatable-cell datatable-cell-sort"><span style="width: 250px;"> STUDENT </span></th>
                                    <th data-field="Country" class="datatable-cell datatable-cell-sort"><span style="width: 130px;">  CATEGORY  </span></th>
                                    <th data-field="ShipDate" class="datatable-cell datatable-cell-sort"><span style="width: 130px;"> JOINED ON </span></th>
                                    <th data-field="ShipDate" class="datatable-cell datatable-cell-sort"><span style="width: 130px;"> Status </span></th>
                                    <th data-field="Actions" data-autohide-disabled="false" class="datatable-cell datatable-cell-sort"><span style="width: 130px;">Actions</span></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                        
@endsection

@section( 'add_on_css' )
<link rel="stylesheet" href="{{ asset('//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css') }}">
<style>
    #stud-table_wrapper>.row {
        width: 100% !important;
    }
    .dataTables_wrapper .dataTable td.orting_asc_disabled:after, .dataTables_wrapper .dataTable td.orting_asc_disabled:before, .dataTables_wrapper .dataTable td.orting_desc_disabled:after, .dataTables_wrapper .dataTable td.orting_desc_disabled:before, .dataTables_wrapper .dataTable td.sorting:after, .dataTables_wrapper .dataTable td.sorting:before, .dataTables_wrapper .dataTable td.sorting_asc:after, .dataTables_wrapper .dataTable td.sorting_asc:before, .dataTables_wrapper .dataTable td.sorting_desc:after, .dataTables_wrapper .dataTable td.sorting_desc:before, .dataTables_wrapper .dataTable th.orting_asc_disabled:after, .dataTables_wrapper .dataTable th.orting_asc_disabled:before, .dataTables_wrapper .dataTable th.orting_desc_disabled:after, .dataTables_wrapper .dataTable th.orting_desc_disabled:before, .dataTables_wrapper .dataTable th.sorting:after, .dataTables_wrapper .dataTable th.sorting:before, .dataTables_wrapper .dataTable th.sorting_asc:after, .dataTables_wrapper .dataTable th.sorting_asc:before, .dataTables_wrapper .dataTable th.sorting_desc:after, .dataTables_wrapper .dataTable th.sorting_desc:before {
       display: none !important;
    }
    .excel_class {
        text-align: right;
        margin-bottom: 8px;
        margin-right: 25px;
    }
    
</style>
@endsection

@section( 'add_on_script' )
    <script src="{{ asset('https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js') }}"></script>
    <script>

        $(document).ready(function() {

            var table = $('#stud-table').DataTable( {
                "processing"    : true,
                "serverSide"    : true,
                "stateSave"     : true,
                "ajax"          : {
                    "url"       : "<?= route( 'students.get_students' ); ?>",
                    "dataType"  : "json",
                    "type"      : "POST",
                    "data"      : function (d) {
                                        d._token = "<?=csrf_token();?>",
                                        d.filter_user_type = $('#filter_user_type').val(),
                                        d.filter_country = $('#filter_country').val(),
                                        d.filter_study_area = $('#filter_study_area').val(),
                                        d.filter_language = $('#filter_language').val(),
                                        d.filter_intake = $('#filter_intake').val(),
                                        d.filter_referral_status = $('#filter_referral_status').val(),
                                        d.search = $('input[type="search"]').val()
                                    }
                },
                "columns"       : [
                    {"data" : "#"},
                    {"data" : "users"},
                    {"data" : "category"},
                    {"data" : "joinedon"},
                    {"data" : "status"},
                    {"data" : "actions" },
                ],
                "columnDefs": [ {
                        'targets': [ 5 ], /* column index [0,1,2,3]*/
                        'orderable': false, /* true or false */
                    }],
                pageLength: 50,
                "language": {
                    "paginate": {
                    "previous": "<i class='ki ki-arrow-back'></i>",
                    "next": "<i class='ki ki-arrow-next'></i>"
                    }
                }
            } );

            $('#filter_user_type, #filter_country, #filter_study_area, #filter_language, #filter_intake, #filter_referral_status').change(function(){
                table.draw();
            });

        } );

        function approve_student( id, status ) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "students.approve" )}}',
                data     : { id:id, status:status },
                beforeSend          : function(){
                    $( "#save_button" ).addClass( "spinner spinner-right" );
                },
                success  : function ( data ) {
                    toastr.success( data.success );
                    setTimeout( function() {
                        
                        window.location.href = "{{route( 'students' )}}";
                        
                    }, 500 );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

        function student_info( student_id ) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "students.modal_info" )}}',
                data     : { student_id : student_id },
                success  : function ( data ) {
                    
                    $( '#firstModal' ).modal();  
                    $( '#firstModal' ).html( data );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }

        function approve_student_status( id, status ) {
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "student.approve_student_status" )}}',
                data     : { id:id, status:status },
                beforeSend          : function(){
                    $( "#spinner" ).show();
                },
                success  : function ( data ) {
                    toastr.success( data.success );
                    $( "#spinner" ).hide();
                    setTimeout( function() {
                        
                        window.location.href = "{{route( 'students' )}}";
                        
                    }, 500 );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }
    </script>
        
@endsection



