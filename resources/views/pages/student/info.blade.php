<div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="firstModalLabel"> {{$title}} </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>
        <div class="modal-body" data-scroll="true">
            <form id="user_form" >
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                @include( 'pages.student._info' )
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>