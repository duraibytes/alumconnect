<div class="card card-custom">
    <!--begin::Form-->
    <div class="card-body">
        
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label> Fname <span class="text-danger">*</span>
                    </label>
                    <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter First Name " value="{{ ( isset( $user_info->fname ) && !empty( $user_info->fname ) ) ? $user_info->fname : '' }}"/>
                    
                </div>
                
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                
                <div class="form-group">
                    <label>Name <span class="text-danger">*</span>
                    </label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name "  value="{{ ( isset( $user_info->name ) && !empty( $user_info->name ) ) ? $user_info->name : '' }}"/>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label> Email <span class="text-danger">*</span>
            </label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Enter email"  value="{{ ( isset( $user_info->email ) && !empty( $user_info->email ) ) ? $user_info->email : '' }}"/>
            <span class="form-text text-muted"> We'll never share your email with anyone else.</span>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="exampleSelect1"> Category </label>
                    <select class="form-control" id="exampleSelect1" name="category">
                        <option value=""> -- select category </option>
                        @if( $category ) 
                            @foreach( $category as $key => $value )
                                @php
                                    $selected           = '';
                                    if( isset( $user_info->category ) && $user_info->category == $value->id ) {
                                        $selected       = 'selected';
                                    }
                                @endphp
                                <option value="{{ $value->id }}" {{$selected}}> {{ $value->name }}</option>    
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="exampleSelect1"> Status <span class="text-danger">*</span></label>
                    <select class="form-control" id="exampleSelect1" name="status">
                        <option value=""> -- select status --  </option>
                               
                        <option value="1" @if( isset( $user_info->status) && $user_info->status == 1 ) selected @endif> ACTIVE </option>
                        <option value="2" @if( isset( $user_info->status) && $user_info->status == 2 ) selected @endif> INACTIVE </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>