@extends( 'layouts.default' )

@section( 'content' )
  <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
     
        @include('layouts.parts.sub_header' )
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        {{-- @include( 'pages.dashboard._datatable_list' ) --}}
                        <table id="user_table" class="table table-bordered table-hover">
                            <thead class="datatable-head">
                                <tr class="datatable-row">
                                    <th data-field="RecordID" class="datatable-cell-left datatable-cell datatable-cell-sort datatable-cell-sorted" data-sort="asc"><span style="width: 10% !important">#</span></th>
                                    <th data-field="OrderID" class="datatable-cell datatable-cell-sort"><span style="width: 20%"> USERS </span></th>
                                    <th data-field="Country" class="datatable-cell datatable-cell-sort"><span style="width: 20%">  CATEGORY  </span></th>
                                    <th data-field="ShipDate" class="datatable-cell datatable-cell-sort"><span style="width: 20%"> LAST LOGGED IN </span></th>
                                    <th data-field="Actions" data-autohide-disabled="false" class="datatable-cell datatable-cell-sort"><span style="width: 30%">Actions</span></th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                        
@endsection

@section( 'add_on_css' )
<link rel="stylesheet" href="{{ asset('//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css') }}">
<style>
    #user_table_wrapper>.row {
        width: 100% !important;
    }
    .dataTables_wrapper .dataTable td.orting_asc_disabled:after, .dataTables_wrapper .dataTable td.orting_asc_disabled:before, .dataTables_wrapper .dataTable td.orting_desc_disabled:after, .dataTables_wrapper .dataTable td.orting_desc_disabled:before, .dataTables_wrapper .dataTable td.sorting:after, .dataTables_wrapper .dataTable td.sorting:before, .dataTables_wrapper .dataTable td.sorting_asc:after, .dataTables_wrapper .dataTable td.sorting_asc:before, .dataTables_wrapper .dataTable td.sorting_desc:after, .dataTables_wrapper .dataTable td.sorting_desc:before, .dataTables_wrapper .dataTable th.orting_asc_disabled:after, .dataTables_wrapper .dataTable th.orting_asc_disabled:before, .dataTables_wrapper .dataTable th.orting_desc_disabled:after, .dataTables_wrapper .dataTable th.orting_desc_disabled:before, .dataTables_wrapper .dataTable th.sorting:after, .dataTables_wrapper .dataTable th.sorting:before, .dataTables_wrapper .dataTable th.sorting_asc:after, .dataTables_wrapper .dataTable th.sorting_asc:before, .dataTables_wrapper .dataTable th.sorting_desc:after, .dataTables_wrapper .dataTable th.sorting_desc:before {
       display: none !important;
    }
</style>
@endsection

@section( 'add_on_script' )
    <script src="{{ asset('https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js') }}"></script>
    <script>

        $(document).ready(function() {

        var filter_status       = $( '#filter_status' ).val();
        var filter_membership   = $( '#filter_membership' ).val();
        $('#user_table').DataTable( {
            "processing"    : true,
            "serverSide"    : true,
            "stateSave"     : true,
            "bProcessing": true,
            "ajax"          : {
                "url"       : "<?= route( 'users.get_users' ); ?>",
                "dataType"  : "json",
                "type"      : "POST",
                "data"      : { "_token" : "<?=csrf_token();?>", "filter_status" : filter_status, "filter_membership" : filter_membership }
            },
            "columns"       : [
                {"data" : "#"},
                {"data" : "users"},
                {"data" : "category"},
                {"data" : "lastloggedin"},
                {"data" : "actions" },
            ],
            "columnDefs": [ {
                    'targets': [ 4 ], /* column index [0,1,2,3]*/
                    'orderable': false, /* true or false */
                }],
            pageLength: 50,
            "language": {
                    "paginate": {
                    "previous": "<i class='ki ki-arrow-back'></i>",
                    "next": "<i class='ki ki-arrow-next'></i>"
                    }
                }
        } );
        } );

        function update_user() {

            var form_data           = new FormData( $( '#user_form' )[0] );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN'  : "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type                : 'POST',
                url                 : '{{route( "users.update" )}}',
                processData         : false,
                contentType         : false,
                data                : form_data,
                dataType            : 'json',
                beforeSend          : function(){
                    $( "#save_button" ).addClass( "spinner spinner-right" );
                },
                success             : function ( data ) {
                    if( data.success ) {
                        toastr.success( data.success );
                        setTimeout( function() {
                            window.location.href = "{{route( 'users.index' )}}";
                        }, 1500 );
                    } else if( data.failure ) {
                        printErrorMsg( data.failure );
                        $( "#save_button" ).removeClass( "spinner spinner-right" );
                    }
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
            }

        

        function edit_user( id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                }
            });
            $.ajax({
                type     : 'POST',
                url      : '{{route( "users.edit" )}}',
                data     : { id:id },
                success  : function ( data ) {
                    
                    $( '#firstModal' ).modal();  
                    $( '#firstModal' ).html( data );
                },
                error: function ( data ) {
                    console.log('Error:', data);
                }
            });
            return true;
        }
    </script>
        
@endsection



