<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Webservices\ApiController;

use App\Models\Student;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
	$student_id 	= $request->user()->id;
	$student_info 	= Student::with( [ 'category_info' ] )->find( $student_id );
    return $student_info;
});

Route::get('category_list/{type?}/{tag?}', [ApiController::class, 'category_list']);
Route::get('subcategory_list/{id}/{type?}/{tag?}', [ApiController::class, 'subcategory_list']);
Route::get('featured_blog/{limit?}/{type?}', [ApiController::class, 'featured_blog']);
Route::get('blog_list/{type?}/{id?}', [ApiController::class, 'blog_list']);
Route::get('blog_info/{id}', [ApiController::class, 'blog_info']);
Route::post('check_student_email', [ApiController::class, 'check_student_email']);
Route::post('authenticate_otp', [ApiController::class, 'authenticate_otp']);
Route::post('create_student', [ApiController::class, 'create_student']);
Route::post('student_avatar_upload', [ApiController::class, 'student_avatar_upload']);
Route::post('referral_student_insert', [ApiController::class, 'referral_student_insert']);
Route::post('change_profile_type', [ApiController::class, 'change_profile_type']);
Route::get('get_category_type_list', [ApiController::class, 'get_category_type_list']);
Route::post('update_student_profile', [ApiController::class, 'update_student_profile']);
Route::post('change_email', [ApiController::class, 'change_email']);
Route::get('confirm_email/{email_unique_key}', [ApiController::class, 'confirm_email']);
Route::post('send_feedback', [ApiController::class, 'send_feedback']);

Route::get('community/{id?}/{limit?}/{type?}/{tag?}', [ApiController::class, 'community']);
Route::get('myuniv/{id?}/{limit?}/{type?}/{tag?}', [ApiController::class, 'my_univ']);