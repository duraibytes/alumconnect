<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use App\Http\Controllers\Webservices\ApiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


 
Auth::routes();
Route::post( '/custom_password', 'Auth\LoginController@custome_password_reset' )->name( 'custom_password' );
Route::get( '/verify_email/{email}', 'MyaccountController@generate_verify_email' )->name( 'verify_email' );
Route::group(['middleware' => 'auth'], function(){
    Route::get( '/', 'HomeController@index' )->name( 'dashboard' );
    Route::get( '/dashboard', 'HomeController@index' )->name( 'home' );
    Route::get( '/list', 'HomeController@list' )->name( 'sample.list' );
    Route::get( '/logout', 'Auth\LoginController@logout' )->name( 'logout' );

    //Students
    Route::get( '/students', 'StudentController@index' )->name( 'students' );
    Route::post( '/students/get_students', 'StudentController@get_students' )->name( 'students.get_students' );
    Route::post( '/students/approve', 'StudentController@approve' )->name( 'students.approve' );
    Route::get( 'export','StudentController@export' )->name( 'export' );
    Route::post( '/students/modal_info', 'StudentController@modal_info' )->name( 'students.modal_info' );
    Route::post( '/student/approve-status', 'StudentController@approve_student_status' )->name( 'student.approve_student_status' );

    //Referrals
    Route::get( '/referrals', 'StudentController@referrals' )->name( 'referrals' );
    Route::post( '/referrals/get_referrals', 'StudentController@get_referrals' )->name( 'referrals.get_referrals' );
    Route::post( '/referrals/change_payment_status', 'StudentController@change_payment_status' )->name( 'referrals.change_payment_status' );
    Route::post( '/referrals/change_referral_status', 'StudentController@change_referral_status' )->name( 'referrals.change_referral_status' );
    Route::post( '/referrals/update_referral_status', 'StudentController@update_referral_status' )->name( 'referrals.update_referral_status' );
   
    //Users
    Route::get( '/users', 'UserController@index' )->name( 'users.index' );
    Route::post( '/users/get_users', 'UserController@get_users' )->name( 'users.get_users' );
    Route::post( '/users/add_users', 'UserController@add_user_form' )->name( 'users.add' );
    Route::post( '/users/insert', 'UserController@insert_user' )->name( 'users.insert' );
    Route::post( '/users/edit_users', 'UserController@edit_user_form' )->name( 'users.edit' );
    Route::post( '/users/update', 'UserController@update_user' )->name( 'users.update' );

    // Blogs
    Route::get( '/post', 'PostController@blog' )->name( 'post' );
    Route::post( '/post/blog_add', 'PostController@add_blog_form' )->name( 'post.blog_add' );
    Route::post( '/post/insert', 'PostController@insert_post' )->name( 'post.insert' );
    Route::get( '/post/edit/{id}', 'PostController@edit_post' )->name( 'post.edit' );
    Route::post( '/post/update', 'PostController@update_post' )->name( 'post.update' );
    Route::delete( '/post/delete_post', 'PostController@delete_post' )->name( 'post.delete_post' );

    // Videos
    Route::get( '/broadcast', 'PostController@video' )->name( 'broadcast' );
    Route::post( '/post/video_add', 'PostController@add_video_form' )->name( 'post.video_add' );

    //Categories
    Route::get( '/category', 'CategoryController@index' )->name( 'category' );
    Route::post( '/category/add', 'CategoryController@add_form' )->name( 'category.add' );
    Route::post( '/category/insert', 'CategoryController@insert_category' )->name( 'category.insert' );
    Route::post( '/category/edit', 'CategoryController@edit_form' )->name( 'category.edit' );
    Route::post( '/category/update', 'CategoryController@update_category' )->name( 'category.update' );

    //My account
    Route::get( '/myaccount', 'MyaccountController@index' )->name( 'myaccount' );

    // Route::get('check_student_email', [ApiController::class, 'check_student_email']);
    Route::get( '/regenerate-password/{email}', 'MyaccountController@password_reset_user' )->name( 'password_reset_user' );
    Route::post( '/change-email', 'MyaccountController@change_email_form' )->name( 'change_email' );
    Route::post( '/email', 'MyaccountController@change_email' )->name( 'email.change' );
    
    Route::post( '/profile/image', 'MyaccountController@profile_photo_change' )->name( 'profilephoto.change' );
});

/* Deep Link Redirect - For mobile redirect to application */
Route::group(['prefix' => 'mobile'], function() {
 
    Route::get('signup/{userId}/{key}', function($userId, $key) {
        return redirect()->away('mobileapp://signup/key?userId=' . $userId . '&amp;key=' . $key);
        die();
    });
});


